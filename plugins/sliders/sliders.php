<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sliders{
  var $CI;
  
  function __construct()
  {
    $this->CI =& get_instance();
  }
  
  function install()
  {
    $this->install_widget();
  }
  
  function install_widget()
  {
    $widgets = array();
    if(file_exists(FCPATH.'plugins/sliders/config/widgets.php'))
      include FCPATH.'plugins/sliders/config/widgets.php';
    if(is_array($widgets) and count($widgets) > 0)
    {
      foreach($widgets as $i => $w)
      {
        $this->CI->widgets->install("sliders",$w);
      }
    }
  }
  
  function setPath()
  {
    $this->CI->load->set_object_path('view',array(FCPATH.'plugins/sliders/views/templates/' => true),false);
    $this->CI->load->set_object_path('library',array(FCPATH.'plugins/sliders/library/' => true),false);
    $this->CI->load->set_object_path('controller',array(FCPATH.'plugins/sliders/controller/' => true),false);
    $this->CI->load->set_object_path('helper',array(FCPATH.'plugins/sliders/helper/' => true),false);
    $this->CI->load->set_object_path('config',array(FCPATH.'plugins/sliders/config/' => true),false);
    $this->CI->load->set_object_path('model',array(FCPATH.'plugins/sliders/model/' => true),false);
  }
  
  function hook_left()
  {
    $this->setPath();
    $output = $this->CI->load->view('maintop',array(),TRUE);
    return $output;
  }
  
  function hook_topcolumn()
  {
    $this->setPath();
    $output = $this->CI->load->view('topcolumn',array(),TRUE);
    return $output;
  }
  
}
