<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sliderscontroller extends Plugins_Controller{

	var $CI;
	var $name = 'sliders';
	var $title = 'Sliders';
	var $description = 'Plugin sliders';

	public function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
	}
  
	public function index()
	{
		$is_ajax = $this->input->post('is_ajax');
		$output = $this->load->view('sliders/views/templates/topcolumn',array(),true);
		return $output;
	}

	function install()
	{

	}

	function uninstall()
	{

	}

	function init()
	{

	}
	

	function setAssets()
	{
		$css = '
				<link rel="stylesheet" href="'.current_admin_theme_url().'dist/bootstrap-3.3.2/css/bootstrap.min.css">
				<link rel="stylesheet" href="'.current_admin_theme_url().'dist/bootstrap-3.3.2/css/bootstrap-theme.min.css">
				<link rel="stylesheet" href="'.current_admin_theme_url().'assets/jquery/jquery-ui-1.11.2/jquery-ui.min.css">
				<link rel="stylesheet" href="'.current_admin_theme_url().'assets/jquery/jquery-ui-1.11.2/jquery-ui.structure.min.css">
				<link rel="stylesheet" href="'.current_admin_theme_url().'assets/jquery/jquery-ui-1.11.2/jquery-ui.theme.css">
				<link rel="stylesheet" href="'.current_admin_theme_url().'assets/css/bootstrap-theme.css">
				<link rel="stylesheet" href="'.current_admin_theme_url().'assets/css/bootstrap-theme-admin.css">';
		$this->CI->assets->add_css($css,"head");

		$js = '
				<script type="text/javascript" src="'.current_admin_theme_url().'assets/jquery/jquery-1.10.2/jquery-1.10.2.min.js"></script>
				<script type="text/javascript" src="'.current_admin_theme_url().'assets/jquery/jquery-ui-1.11.2/jquery-ui.min.js"></script>
				<!-- <script type="text/javascript" src="'.current_admin_theme_url().'dist/bootstrap-3.3.2/js/bootstrap.min.js"></script> -->
				<script type="text/javascript" src="'.current_admin_theme_url().'assets/jquery/tinymce_1.4/tinymce.min.js"></script>
				<script type="text/javascript" src="'.current_admin_theme_url().'assets/jquery/tinymce_1.4/jquery.tinymce.min.js"></script>
				<script type="text/javascript" src="'.current_admin_theme_url().'assets/js/jquery.form.js"></script>
				<script type="text/javascript" src="'.current_admin_theme_url().'assets/js/ajax.js"></script>
				<script type="text/javascript" src="'.current_admin_theme_url().'assets/js/data.js"></script>
				<script type="text/javascript" src="'.current_admin_theme_url().'assets/js/blocks.js"></script>
				<script type="text/javascript" src="'.current_admin_theme_url().'assets/js/script.js"></script>			
				';
		$this->CI->assets->add_js($js,"head");

		$js = '';
		$this->CI->assets->add_js($js,"body");
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
