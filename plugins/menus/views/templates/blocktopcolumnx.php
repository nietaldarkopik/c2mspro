<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
            <nav class="navbar navbar-inverse" role="banner">
              <div class="container">
                <div class="row">
                  <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".user-navbar-collapse">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand" href="index.html"><img src="<?php echo current_theme_url();?>assets/images/logo.png" alt="logo" class="img-responsive"></a>
                  </div>
                  <div class="collapse navbar-collapse user-navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="full-width.html">Full Width</a></li>
                        <li><a href="left-bar.html">Left Bar</a></li>
                        <li><a href="right-bar.html">Right Bar</a></li>
                        <li class="active" class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <i class="glyphicon glyphicon-angle-down"></i></a>
                          <ul class="dropdown-menu">
                              <li><a href="three-columns.html">Three Columns</a></li>
                              <li class="active"><a href="four-columns.html">Four Columns</a></li>
                          </ul>
                        </li>                    
                    </ul>
                  </div>
                </div>
              </div>
            </nav>
          
