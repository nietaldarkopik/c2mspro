<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class menus{
  var $CI;
  
  function __construct()
  {
    $this->CI =& get_instance();
  }
  
  function setPath()
  {
    $this->CI->load->set_object_path('view',array(FCPATH.'plugins/menus/views/templates/' => true),false);
    $this->CI->load->set_object_path('library',array(FCPATH.'plugins/menus/library/' => true),false);
    $this->CI->load->set_object_path('controller',array(FCPATH.'plugins/menus/controller/' => true),false);
    $this->CI->load->set_object_path('helper',array(FCPATH.'plugins/menus/helper/' => true),false);
    $this->CI->load->set_object_path('config',array(FCPATH.'plugins/menus/config/' => true),false);
    $this->CI->load->set_object_path('model',array(FCPATH.'plugins/menus/model/' => true),false);
  }
  
  function hook_top()
  {
    $this->setPath();
    $output = $this->CI->load->view('top',array(),TRUE);
    return $output;
  }
  function hook_topcolumn()
  {
    $this->setPath();
    $output = $this->CI->load->view('top',array(),TRUE);
    return $output;
  }
}
