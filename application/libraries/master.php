<?php

class Master{
	
	var $CI;
	
	function Master(){
		$this->CI =& get_instance();
	}
	
	function get_nilai()
	{
		$q = $this->CI->db->get("m_nilai");
		$nilai = $q->result_array();
		return $nilai;
	}
	function get_assesment()
	{
		$q = $this->CI->db->get("m_assesment");
		$nilai = $q->result_array();
		return $nilai;
	}
	
	function get_value($table = "",$field = "",$where = "")
	{
		$this->CI->db->where($where);
		$q = $this->CI->db->get($table);
		
		$output = "";
		if($q->num_rows() == 0)
			return $output;
			
		$data = $q->row_array();
		$output = (isset($data[$field]))?$data[$field]:"";
		
		return $output;
	}
  
  
  function get_inline_edit_content($controller = "",$function = "%",$param = "%",$position = "top",$is_editable = "auto",$default_content = "")
  {
    $sess_is_editable = $this->get_value(TABLE_SYS_SETTINGS,"value",array("setting_id" => '1'));
    $sess_is_editable = ($sess_is_editable == "active")?true:false;
    
    $is_editable = ($is_editable == "auto" and !empty($sess_is_editable))?$sess_is_editable:false;

    $output = "";
    $page_url = array();
    if(!empty($controller) and $controller != '%')
      $page_url[] = $controller;
    if(!empty($function) and $function != '%')
      $page_url[] = $function;
    if(!empty($param) and $param != '%')
      $page_url[] = $param;
    $page_url = implode("/",$page_url);
    $q = $this->CI->db->query("SELECT * FROM plugin_contents WHERE page_url LIKE '%".$page_url."%' AND position = '".$position."' AND status = 'Active'");
    $content = $q->row_array();

    $content['page_description'] = (isset($content['page_description']) and !empty($content['page_description']))?$content['page_description']:$default_content;
    $user_level = $this->CI->user_access->get_level();
    if($is_editable == true and $user_level == '1')
    {
      $page_description = (isset($content['page_description']))?$content['page_description']:"Klik disini untuk merubah konten";
      $cposition = (isset($content['position']))?$content['position']:"";
      $cpage_url = (isset($content['page_url']))?$content['page_url']:"";
      $page_id = (isset($content['page_id']))?$content['page_id']:"";
      $id = $page_id;
      $id = ($id == "-")?"":'id="content-'.$id.'"';
      $output = '<form method="post" action="'.base_url('services/inline_edit/save_page/'.$page_id).'" class="inline_editable_form">
                  <div class="inline_editable" title="Klik disini untuk merubah konten">
                    <div class="editable" '.$id.'>'.($page_description).'</div>
                  </div>
                  <input type="hidden" name="page_url" value="'.$page_url.'"/>
                  <input type="hidden" name="position" value="'.$position.'"/>
                  <input type="hidden" name="nonce" value="'.microtime().'"/>
                  <textarea name="page_description" class="hide">'.htmlentities($page_description).'</textarea>
                 </form>';
    }else{
      $output = (isset($content['page_description']))?$content['page_description']:"";
    }
    return $output;
  }
}
