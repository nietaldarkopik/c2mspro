<?php

class Input_button{
  var $CI;
  var $form_field = "";
  function Input_button()
  {
		$this->CI =& get_instance();
  }
  
  function config($field = array())
  {
      $the_config = array();
      $output = "";
      $type = "button";
      
      $star_required = (isset($field['star_required']))?$field['star_required']:"";
      $label = (isset($field['label']))?$field['label']:"";
      $name = (isset($field['name']))?$field['name']:"";
      $id = (isset($field['id']))?$field['id']:"";
      $value = (isset($field['value']) and $field['value'] != "" )?$field['value']:"";
      $maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
      $size = (isset($field['size']))?$field['size']:"";
      $style = (isset($field['style']))?$field['style']:"";
      $class = (isset($field['class']))?$field['class']:"";
      $parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
      $input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
      $rules = (isset($field['rules']))?$field['rules']:"";
      $class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
      $class .= ' btn btn-primary btn-block';
      $content = (isset($field['content']))?$field['content']:"";
      
      $data = array(
        'name'        => $input_name,
        'id'          => $id,
        'value'       => $value,
        'maxlength'   => $maxlength,
        'size'        => $size,
        'style'       => $style,
        'type'       => $type,
        'class'		=> $class,
        'content' => $content
      );

      $field_attributes = $field;
      $field_attributes = array_merge($field_attributes,$data);

      foreach($field_attributes as $i => $o)
      {
        if(is_array($o) and count($o) >  0)
        {
          unset($field_attributes[$i]);
        }
      }

      $label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
      #$output .= '<label for="'. $name .'">'. $label .'</label>';
      
      $data['content'] = (empty($data['content']))?$label:$data['content'];
      $output .= '
        <br/>
    		<div class="row">
          <div class="col-lg-3">
            <div class="input-group">
              &nbsp;
            </div>
				  </div>
				  <div class="col-lg-6">
						'.form_button($data).'
				  </div>
          <div class="col-lg-3">
            <div class="input-group">
              &nbsp;
            </div>
				  </div>
				</div>';
    $this->output_field = $output;
  }
  
  function output()
  {
    return $this->output_field;
  }
}

