<?php

class Flotchart{
	var $CI;
	
	function Flotchart(){
		$this->CI =& get_instance();
	}
	
	function config($configs = array())
	{
	
	}
	
	function output($placeholder = '#placeholder',$data = "")
	{
		if(empty($data))
			return "";
		
		$output = "
		<script type='text/javascript' src='".base_url()."themes/".CURRENT_THEME."/static/js/flot/jquery.flot.js'></script>
		<script type='text/javascript' src='".base_url()."themes/".CURRENT_THEME."/static/js/flot/excanvas.js'></script>
		<script type='text/javascript' src='".base_url()."themes/".CURRENT_THEME."/static/js/flot/jquery.flot.pie.js'></script>
		<script type='text/javascript' src='".base_url()."themes/".CURRENT_THEME."/static/js/flot/jquery.flot.pie.js'></script>
		<script type='text/javascript' src='".base_url()."themes/".CURRENT_THEME."/static/js/flot/static/js/flot/jquery.flot.categories.js'></script>
		<script type='text/javascript' src='".base_url()."themes/".CURRENT_THEME."/static/js/flot/static/js/flot/jquery.flot.canvas.js'></script>
		<script type='text/javascript' src='".base_url()."themes/".CURRENT_THEME."/static/js/flot/static/js/flot/jquery.flot.crosshair.js'></script>
		<script type='text/javascript' src='".base_url()."themes/".CURRENT_THEME."/static/js/flot/static/js/flot/jquery.flot.errorbars.js'></script>
		<script type='text/javascript' src='".base_url()."themes/".CURRENT_THEME."/static/js/flot/static/js/flot/jquery.flot.fillbetween.js'></script>
		<script type='text/javascript' src='".base_url()."themes/".CURRENT_THEME."/static/js/flot/static/js/flot/jquery.flot.image.js'></script>
		<script type='text/javascript' src='".base_url()."themes/".CURRENT_THEME."/static/js/flot/static/js/flot/jquery.flot.navigate.js'></script>
		<script type='text/javascript' src='".base_url()."themes/".CURRENT_THEME."/static/js/flot/static/js/flot/jquery.flot.resize.js'></script>
		<script type='text/javascript' src='".base_url()."themes/".CURRENT_THEME."/static/js/flot/static/js/flot/jquery.flot.stack.js'></script>
		<script type='text/javascript' src='".base_url()."themes/".CURRENT_THEME."/static/js/flot/static/js/flot/jquery.flot.symbol.js'></script>
		<script type='text/javascript' src='".base_url()."themes/".CURRENT_THEME."/static/js/flot/static/js/flot/jquery.flot.threshold.js'></script>
		<script type='text/javascript' src='".base_url()."themes/".CURRENT_THEME."/static/js/flot/static/js/flot/jquery.flot.time.js'></script>
		";
		
		$output .= "var data = ".json_decode($data,true)."\n";
		$output .= "$.plot('".$placeholder."', data, {
							series: {
								pie: {
									show: true,
									radius: 1,
									label: {
										show: true,
										radius: 3/4,
										formatter: labelFormatter,
										background: {
											opacity: 0.5
										}
									}
								}
							},
							legend: {
								show: false
							}
						});";
	}
}
