<?php

class Gts_notification{
  var $CI;

  function Gts_notification()
  {
		$this->CI =& get_instance();
    $this->CI->load->library("gts/gts_members");
    $this->CI->load->library("gts/gts_certificates");
    $this->CI->load->library("gts/gts_greenwallet");
  }

  function get_all_notification($member_code = "")
  {
    if(empty($member_code))
      $member_code = $this->CI->gts_members->get_current_member_code();
    
    $q_notif = $this->CI->db->query("
    SELECT 'bidding' as type_notification,
      bidding_id as id,
      selling_id,
      bid_code,
      member_id,
      member_code,
      certificate_id,
      no_certificate,
      no_reg_certificate,
      certificate_member_id,
      certificate_member_code,
      amount,
      status_owner,
      date_member_approved,
      status_admin,
      date_admin_approved,
      date_bidding,
      date_inserted,
      date_updated FROM `addon_bidding` 
    WHERE certificate_owner_code = '".$member_code."'
    ORDER BY date_updated DESC");
/*
      UNION 
    SELECT 'selling' as type_notification,
      selling_id as id,
      selling_id as selling_id,
      sell_code,
      member_id,
      member_code,
      certificate_id,
      no_certificate,
      no_reg_certificate,
      certificate_member_id,
      certificate_member_code,
      amount,
      status_owner,
      date_member_approved,
      status_admin,
      date_admin_approved,
      date_selling,
      date_inserted,
      date_updated FROM `addon_selling`
*/
    $notifications = $q_notif->result_array();
    
    return $notifications;
  }
  
  function get_notification_unread($member_code = "")
  {
    if(empty($member_code))
      $member_code = $this->CI->gts_members->get_current_member_code();
    
    $q_notif = $this->CI->db->query("
    SELECT * FROM `addon_bidding` LEFT JOIN `addon_notify_status` ON `addon_bidding`.bidding_id = `addon_notify_status`.object_id
    WHERE certificate_owner_code = '".$member_code."' AND addon_notify_status.status IS NULL
    ORDER BY addon_bidding.date_updated DESC");
    $notifications = $q_notif->result_array();
    
    return $notifications;
  }
  
  function mark_as_read($object_id = '',$type = 'bidding',$member_id = "")
  {
    if(empty($member_id))
      $member_id = $this->CI->gts_members->get_current_member_id();
      
    if(empty($object_id))
      return false;
      
    $check = array( 'object_id' => $object_id,
                    'type' => $type,
                    'status' => 'read',
                    'member_id' => $member_id
                  );
                  
    $data = $check;
    $data['date_updated'] = date('Y-m-d H:i:s');
    
    $this->CI->db->where($check);
    $q_check = $this->CI->db->get('addon_notify_status');
    
    if($q_check->num_rows() == 0)
    {
      $q_insert = $this->CI->db->insert('addon_notify_status',$data);
      return $q_insert;
    }
    return false;
  }
  
  function is_read($object_id = '',$type = 'bidding',$member_id = "")
  {
    if(empty($member_id))
      $member_id = $this->CI->gts_members->get_current_member_id();
      
    if(empty($object_id))
      return false;
      
    $check = array( 'object_id' => $object_id,
                    'type' => $type,
                    'status' => 'read',
                    'member_id' => $member_id
                  );
    
    $this->CI->db->where($check);
    $q_check = $this->CI->db->get('addon_notify_status');
    
    return $q_check->num_rows();
  }
}
