<?php

class Gts_certificates{
  var $CI;

  function Gts_certificates()
  {
		$this->CI =& get_instance();
    $this->CI->load->library("gts/gts_members");
  }

  function is_sale($certificate_id = "")
  {
    if(empty($certificate_id))
      return false;
      
    $this->CI->db->where(array("status_owner" => "","certificate_id" => $certificate_id));
    $qcek = $this->CI->db->get("addon_selling");
    if($qcek->num_rows() == 0)
      return false;
    else
    {
      $fselling = $qcek->row_array();
      return $fselling;
    }
    return false;
  }

  function is_owned($certificate_id = "",$member_code = "")
  {
    if(empty($certificate_id))
      return false;

    if(empty($member_code))
      $member_code = $this->CI->gts_members->get_current_member_code();

    $is_owned = $this->CI->db->query("SELECT * FROM addon_reg_certificates WHERE addon_reg_certificates.owning_status='active' AND addon_reg_certificates.member_code = '".$member_code."' AND addon_reg_certificates.certificate_id = '".$certificate_id."'");
    
    return $is_owned->num_rows();
  }
  
  function is_bid($selling_id = "",$member_code = "")
  {
    if(empty($selling_id))
      return false;
    
    $member_code = (empty($member_code))?$this->gts_members->get_current_member_code():$member_code;
    $this->CI->db->where(array("status_owner" => "","selling_id" => $selling_id,"member_code" => $member_code));
    $qcek = $this->CI->db->get("addon_bidding");
    
    if($qcek->num_rows() == 0)
      return false;
    else
    {
      $fselling = $qcek->row_array();
      return $fselling;
    }
    return false;
  }
  
  function get_seller($selling_id = "")
  {
    if(empty($selling_id))
      return false;
      
    $this->CI->db->where(array("selling_id" => $selling_id));
    $qcek = $this->CI->db->get("addon_selling");
    if($qcek->num_rows() == 0)
      return false;
    else
    {
      $fselling = $qcek->row_array();
      return $fselling;
    }
    return false;
  }
  
  function get_bidder($bidding_id = "")
  {
    if(empty($bidding_id))
      return false;
      
    $this->CI->db->where(array("bidding_id" => $bidding_id));
    $qcek = $this->CI->db->get("addon_bidding");
    if($qcek->num_rows() == 0)
      return false;
    else
    {
      $fbidding = $qcek->row_array();
      return $fbidding;
    }
    return false;
  }
  
  function do_sell($certificate_id = "")
  {
    if(empty($certificate_id))
      return false;
      $member_code = $this->CI->gts_members->get_current_member_code();
      $is_owned = $this->CI->db->query("SELECT * FROM addon_reg_certificates WHERE addon_reg_certificates.owning_status='active' AND addon_reg_certificates.member_code = '".$member_code."' AND addon_reg_certificates.certificate_id = '".$certificate_id."'");
      
      if($is_owned->num_rows() == 1)
      {
        if($this->is_sale($certificate_id) === false)
        {
          $certificate = $this->get_by_id($certificate_id);
          
          $qori_certificate = $this->CI->db->query("SELECT * FROM addon_certificates WHERE addon_certificates.certificate_id = '".$certificate_id."'");
          $ori_certificate = $qori_certificate->row_array();
          
          $ori_member = array();
          if(isset($ori_certificate['member_code']))
          {
            $qori_certificate = $this->CI->db->query("SELECT * FROM addon_member WHERE addon_member.plain_code = '".$ori_certificate['member_code']."'");
            $ori_member = $qori_certificate->row_array();
          }
          
          if(is_array($certificate) and count($certificate) > 0 and isset($certificate['certificate_id']))
          {
            $data_selling = array(
            'sell_code' => 'S'.strtotime(date("Y-m-d H:i:s")),
            'member_id' => $this->CI->gts_members->get_current_member_id(),
            'member_code' => $certificate['member_code'],
            'certificate_id' => $certificate['certificate_id'],
            'no_certificate' => $certificate['nomor_sertifikat'],
            'no_reg_certificate' => $certificate['no_registrasi'],
            'certificate_member_id' => (isset($ori_member['member_id']))?$ori_member['member_id']:'',
            'certificate_member_code' => (isset($ori_member['plain_code']))?$ori_member['plain_code']:'',
            'amount' => '',
            'status_owner' => '',
            'date_member_approved' => '',
            'status_admin' => '',
            'date_admin_approved' => '',
            'date_selling' => date("Y-m-d H:i:s"),
            'date_inserted' => date("Y-m-d H:i:s"),
            'date_updated' => date("Y-m-d H:i:s")
            );
          }
          
          $qcek = $this->CI->db->insert("addon_selling",$data_selling);
          return $qcek;
        }
    }
    return false;
  }
  
  function do_cancel_sell($selling_id = "")
  {
    if(empty($selling_id))
      return false;
      
      $seller = $this->get_seller($selling_id);
      $member_code = $this->CI->gts_members->get_current_member_code();
      if(!isset($seller['member_code']) or !isset($seller['certificate_id']) or $member_code != $seller['member_code'])
        return false;
      
      $certificate_id = $seller['certificate_id'];
      $is_owned = $this->CI->db->query("SELECT * FROM addon_reg_certificates WHERE addon_reg_certificates.owning_status='active' AND addon_reg_certificates.member_code = '".$member_code."' AND addon_reg_certificates.certificate_id = '".$certificate_id."'");
      
      if($is_owned->num_rows() == 1)
      {
        if($this->is_sale($certificate_id) !== false)
        {
          $data_selling = array(
            'status_owner' => 'cancel',
            'date_updated' => date("Y-m-d H:i:s")
          );
          
          $this->CI->db->where(array('selling_id' => $selling_id));
          $qcek = $this->CI->db->update("addon_selling",$data_selling);
          return $qcek;
        }
    }
    return false;
  }
  
  function do_bidding($selling_id = "",$amount = 0)
  {
    if(empty($selling_id) or empty($amount) or !is_numeric($amount) or $amount < 1000)
      return false;
    
    $member_code = $this->CI->gts_members->get_current_member_code();
    $current_gw = $this->CI->gts_greenwallet->get_current_amount_gw();
    
    if($current_gw < $amount)
      return false;
    
    $seller = $this->get_seller($selling_id);
    $certificate_id = $seller['certificate_id'];
    $is_sale = $this->is_sale($certificate_id);
   
    if($is_sale !== false and isset($is_sale['certificate_id']) and isset($is_sale['selling_id']) and $is_sale['selling_id'] == $selling_id)
    {
      $certificate_id = $is_sale['certificate_id'];
      $certificate = $this->get_by_id($certificate_id);
      $qori_certificate = $this->CI->db->query("SELECT * FROM addon_certificates WHERE addon_certificates.certificate_id = '".$certificate_id."'");
      $ori_certificate = $qori_certificate->row_array();
      
      $cert_owner_member = $this->CI->gts_certificates->get_owner_certificate($certificate_id);
      
      $ori_member = array();
      if(isset($ori_certificate['member_code']))
      {
        $qori_certificate = $this->CI->db->query("SELECT * FROM addon_member WHERE addon_member.plain_code = '".$ori_certificate['member_code']."'");
        $ori_member = $qori_certificate->row_array();
      }
     
      if(is_array($certificate) and count($certificate) > 0 and isset($certificate['certificate_id']))
      {
        $data_bidding = array(
          'certificate_id' => $certificate_id,
          'selling_id' => $selling_id,
          'bid_code' => 'B'.strtotime(date("Y-m-d H:i:s")),
          'member_id' => $this->CI->gts_members->get_current_member_id(),
          'member_code' => $member_code,
          'no_certificate' => $certificate['nomor_sertifikat'],
          'no_reg_certificate' => $certificate['no_registrasi'],
          'certificate_owner_id' => (isset($cert_owner_member['member_id']))?$cert_owner_member['member_id']:'',
          'certificate_owner_code' => (isset($cert_owner_member['plain_code']))?$cert_owner_member['plain_code']:'',
          'certificate_member_id' => (isset($ori_member['member_id']))?$ori_member['member_id']:'',
          'certificate_member_code' => (isset($ori_member['plain_code']))?$ori_member['plain_code']:'',
          'amount' => $amount,
          'status_owner' => '',
          'date_member_approved' => '',
          'status_admin' => '',
          'date_admin_approved' => '',
          'date_bidding' => date("Y-m-d H:i:s"),
          'date_inserted' => date("Y-m-d H:i:s"),
          'date_updated' => date("Y-m-d H:i:s")
        );
        
        $qcek = $this->CI->db->insert("addon_bidding",$data_bidding);
        return $qcek;
      }
    }
    return false;
  }
  
  function do_cancel_bidding($bidding_id = "")
  {
    if(empty($bidding_id))
      return false;
  
      $data_bidding = array(
        'status_owner' => 'cancel',
        'date_updated' => date("Y-m-d H:i:s")
      );
      
      $this->CI->db->where(array('bidding_id' => $bidding_id));
      $qcek = $this->CI->db->update("addon_bidding",$data_bidding);
      return $qcek;
  }
  
  function get_by_id($certificate_id = "")
  {
    if(empty($certificate_id))
      return false;
      
    $query = $this->CI->db->query("SELECT * FROM addon_certificates LEFT JOIN addon_clusters ON addon_certificates.cluster_id = addon_clusters.cluster_id WHERE addon_certificates.certificate_id = '".$certificate_id."'");
    $rows = $query->row_array();
    
    return $rows;
  }
  
  function get_by_member_code($member_code = "")
  {
    if(empty($member_code))
      return false;
      
    $query = $this->CI->db->query("SELECT * FROM addon_certificates LEFT JOIN addon_clusters ON addon_certificates.cluster_id = addon_clusters.cluster_id WHERE addon_certificates.member_code = '".$member_code."'");
    $rows = $query->row_array();
    
    return $rows;
  }
  
  function get_by_owner_code($member_code = "")
  {
    if(empty($member_code))
      return false;
      
    $query = $this->CI->db->query("SELECT * FROM addon_reg_certificates LEFT JOIN addon_clusters ON addon_reg_certificates.cluster_id = addon_clusters.cluster_id WHERE addon_reg_certificates.owning_status='active' AND addon_reg_certificates.member_code = '".$member_code."'");
    if($query->num_rows() == 0)
    {
      $query = $this->CI->db->query("SELECT * FROM addon_certificates LEFT JOIN addon_clusters ON addon_certificates.cluster_id = addon_clusters.cluster_id WHERE addon_certificates.member_code = '".$member_code."'");
    }
    $rows = $query->result_array();
    return $rows;
  }
  
  function get_owner_certificate($certificate_id = "")
  {
    if(empty($certificate_id))
      return false;
      
    $query = $this->CI->db->query("SELECT * FROM addon_reg_certificates LEFT JOIN addon_clusters ON addon_reg_certificates.cluster_id = addon_clusters.cluster_id WHERE addon_reg_certificates.owning_status='active' AND addon_reg_certificates.certificate_id = '".$certificate_id."' AND addon_reg_certificates.owning_status = 'active'");
    if($query->num_rows() == 0)
    {
      $query = $this->CI->db->query("SELECT * FROM addon_certificates LEFT JOIN addon_clusters ON addon_certificates.cluster_id = addon_clusters.cluster_id WHERE addon_reg_certificates.certificate_id = '".$certificate_id."'");
    }
    $rows = $query->row_array();
    $owner = array();
    if(isset($rows['member_code']))
      $owner = $this->CI->gts_members->get_member_by_code($rows['member_code']);
    return $owner;
  }
  
  function get_on_sales()
  {
    $query = $this->CI->db->query("SELECT * FROM addon_member,addon_selling,addon_reg_certificates WHERE addon_member.member_id = addon_selling.member_id AND addon_member.plain_code = addon_reg_certificates.member_code AND addon_selling.certificate_id=addon_reg_certificates.certificate_id  AND addon_selling.member_code=addon_reg_certificates.member_code AND addon_reg_certificates.owning_status='active' AND addon_selling.status_owner = '' ORDER BY date_selling DESC");
    $rows = $query->result_array();
    
    return $rows;
    
  }
  
  function get_on_agree($curr_member_code = "")
  {
    if(empty($curr_member_code))
      $curr_member_code = $this->CI->gts_members->get_current_member_code();
      
    $query = $this->CI->db->query("SELECT * FROM addon_member,addon_bidding,addon_reg_certificates WHERE addon_member.member_id = addon_bidding.certificate_owner_id AND addon_member.plain_code = addon_reg_certificates.member_code AND addon_bidding.certificate_id=addon_reg_certificates.certificate_id  AND addon_bidding.certificate_owner_code=addon_reg_certificates.member_code AND addon_reg_certificates.owning_status='active' AND addon_bidding.status_owner = 'approve' AND addon_bidding.member_code = '".$curr_member_code."' ORDER BY date_member_approved DESC");
    $rows = $query->result_array();
    
    return $rows;
    
  }
  
  function get_history_by_id($certificate_id = "")
  {
    if(empty($certificate_id))
      return false;
      
    $query = $this->CI->db->query("SELECT * FROM addon_reg_certificates WHERE certificate_id = '".$certificate_id."' ORDER BY owning_date DESC");
    $rows = $query->result_array();
    
    return $rows;
    
  }
  
  function get_my_certificates()
  {
    $current_member_code = $this->CI->gts_members->get_current_member_code();
    $certificates = $this->get_by_owner_code($current_member_code);
    return $certificates;
  }
  
  function get_total_bid($selling_id = "")
  {
    if(empty($selling_id))
      return 0;
      
    $qtotal = $this->CI->db->query("SELECT * FROM addon_bidding WHERE selling_id = '".$selling_id."'");
    return $qtotal->num_rows();
  }
  
  function get_all_bidders($selling_id = "")
  {
    if(empty($selling_id))
      return false;
            
    $this->CI->db->where(array("selling_id" => $selling_id));
    $qcek = $this->CI->db->get("addon_bidding");
    if($qcek->num_rows() == 0)
      return false;
    else
    {
      $fbidding = $qcek->result_array();
      return $fbidding;
    }
    return false;
  }
  
  function get_following($member_id = "")
  {
    if(empty($member_id))
      $member_id = $this->CI->gts_members->get_current_member_id();
            
    $qcek = $this->CI->db->query("SELECT * FROM addon_bidding WHERE member_id = '".$member_id."' AND (status_owner = '' or status_owner = 'approve')");
    
    if($qcek->num_rows() == 0)
      return false;
    else
    {
      $fbidding = $qcek->result_array();
      return $fbidding;
    }
    return false;
    
  }

  function get_approved_bidder($selling_id = "")
  {
    if(empty($selling_id))
      return false;
    
    $q_get_approved = $this->CI->db->query("SELECT addon_bidding.*,addon_selling.sell_code,addon_selling.date_selling FROM addon_bidding,addon_selling WHERE addon_bidding.selling_id = addon_selling.selling_id AND addon_bidding.selling_id = '".$selling_id."' AND addon_bidding.status_owner = 'approve' ");
    if($q_get_approved->num_rows() == 1)
    {
      $f_approved = $q_get_approved->row_array();
      return $f_approved;
    }
    
    return false;
  }
}
