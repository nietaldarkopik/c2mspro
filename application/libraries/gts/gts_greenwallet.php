<?php

class Gts_greenwallet{
  var $CI;

  function Gts_greenwallet()
  {
		$this->CI =& get_instance();
  }
  
  function get_members()
  {
    $q_member = $this->CI->db->get("addon_green_wallet");
    $f_member = $q_member->result_array();
    return $f_member;
  }
  
  function get_member_detail($user_id = "")
  {
    if(empty($user_id))
      return array();
      
    $this->CI->db->where(array('user_id' => $user_id));
    $q_member = $this->CI->db->get("addon_green_wallet");
    $f_member = $q_member->row_array();
    return $f_member;
  }
  
  function get_phone_account($member_code = "current")
  {
    if(empty($member_code))
      return false;
    
    if($member_code == "current")
      $member_code = $this->get_current_member_code();
      
    $member = $this->get_member_by_code($member_code);
    $phone_account = (isset($member['phone_account']))?$member['phone_account']:"";
    return $phone_account;
  }
  
  function get_member_by_code($member_code = "")
  {
    if(empty($member_code))
      return array();
      
    $this->CI->db->where(array('member_code' => $member_code));
    $q_member = $this->CI->db->get("addon_green_wallet");
    $f_member = $q_member->row_array();
    return $f_member;
  }
  
  function get_current_member()
  {
    $user_id = $this->CI->user_access->current_user_id;
    $member = $this->get_member_detail($user_id);
    return $member;
  }
  
  function get_member_code($user_id = "")
  {
    $member = $this->get_member_detail($user_id);
    $member_code = (isset($member['member_code']))?$member['member_code']:"";
    return $member_code;
  }
  
  function get_current_member_code()
  {
    $user_id = $this->CI->user_access->current_user_id;
    $member = $this->get_member_detail($user_id);
    $member_code = (isset($member['member_code']))?$member['member_code']:"";
    return $member_code;
  }
  
  function get_current_green_wallet_id()
  {
    $user_id = $this->CI->user_access->current_user_id;
    $member = $this->get_member_detail($user_id);
    $member_code = (isset($member['green_wallet_id']))?$member['green_wallet_id']:"";
    return $member_code;
  }
  
  function get_current_amount_gw()
  {
    $user_id = $this->CI->user_access->current_user_id;
    $member = $this->get_member_detail($user_id);
    $amount_gw = (isset($member['amount_gw']))?$member['amount_gw']:"";
    return $amount_gw;    
  }
  
  function get_tmp_current_amount_gw()
  {
    $total_current_amount = $this->get_current_amount_gw();
    $total_current_amount_bidded = $this->get_current_amount_bidded();
    
    $tmp_current_amount_gw = $total_current_amount - $total_current_amount_bidded;
    return $tmp_current_amount_gw;
  }
  
  function get_current_amount_bidded()
  {
    $my_all_bids = $this->CI->gts_certificates->get_following();
    $total_amount_bids = 0;
    if(is_array($my_all_bids) and count($my_all_bids) > 0)
    {
      foreach($my_all_bids as $i => $b)
      {
        $total_amount_bids += $b['amount'];
      }
    }
    return $total_amount_bids;
  }
  
  function is_registered($user_id = "")
  {
    if(empty($user_id))
      $user_id = $this->CI->user_access->current_user_id;
    
    $member = $this->get_member_detail($user_id);
    $member_code = (isset($member['green_wallet_id']))?$member['green_wallet_id']:"";
    return $member_code;
  }
  
  function register($data = array())
  {
    $q_insert_gw_member = $this->CI->db->insert("addon_green_wallet",$data);
    
    $data_reg = array(
                        'member_id' => (isset($data['member_id']))?$data['member_id']:0,
                        'member_code' => (isset($data['member_code']))?$data['member_code']:0,
                        'bidding_id' => '0',
                        'bidding_code' => '',
                        'selling_id' => '0',
                        'selling_code' => '',
                        'certificate_id' => '0',
                        'amount' => (isset($data['amount_gw']))?$data['amount_gw']:0,
                        'status' => 'first_load',
                        'date_transaction' => date("Y-m-d H:i:s"),
                        'date_inserted' => date("Y-m-d H:i:s")
                      );
    $this->CI->db->insert("addon_reg_green_wallet",$data_reg);
    return $q_insert_gw_member;
  }
  
  function save_transaction($data = array())
  {
    if(!isset($data['phone_from']) or empty($data['phone_from']))
      return false;
    if(!isset($data['phone_to']) or empty($data['phone_to']))
      return false;
    if(!isset($data['selling_id']) or empty($data['selling_id']))
      return false;
    if(!isset($data['certificate_id']) or empty($data['certificate_id']))
      return false;
    if(!isset($data['bidding_id']) or empty($data['bidding_id']))
      return false;
    if(!isset($data['selling_id']) or empty($data['selling_id']))
      return false;
    if(!isset($data['amount']) or empty($data['amount']))
      return false;
    $data_save = array(
                  'phone_from' => (isset($data['phone_from']))?$data['phone_from']:'',
                  'phone_to' =>  (isset($data['phone_to']))?$data['phone_to']:'',
                  'transaction_code' =>  (isset($data['transaction_code']))?$data['transaction_code']:'',
                  'certificate_id' =>  (isset($data['certificate_id']))?$data['certificate_id']:'',
                  'bidding_id' =>  (isset($data['bidding_id']))?$data['bidding_id']:'',
                  'selling_id' =>  (isset($data['selling_id']))?$data['selling_id']:'',
                  'amount' =>  (isset($data['amount']))?$data['amount']:'',
                  'status' =>  (isset($data['status']))?$data['status']:'waiting',
                  'date_transaction' =>  (isset($data['date_transaction']))?$data['date_transaction']:date("Y-m-d H:i:s"),
                  'date_paid' =>  (isset($data['date_paid']))?$data['date_paid']:'0000-00-00 00:00:00',
                  'date_inserted' =>  (isset($data['date_inserted']))?$data['date_inserted']:date("Y-m-d H:i:s"),
                  'description' =>  (isset($data['description']))?$data['description']:'',
                  'response' =>  (isset($data['response']))?$data['response']:''
                  );
      $r = $this->CI->db->insert("addon_transaction",$data_save);
      return $r;
  }
  
  function remove_transaction($selling_id = "",$bidding_id = "")
  {
    $this->CI->db->where(array('selling_id' => $selling_id,'bidding_id' => $bidding_id));
    $r = $this->CI->db->delete("addon_transaction");
    return $r;
  }
  
  function set_to_paid($selling_id = "",$bidding_id = "")
  {
    
    
    #save data transaction table addon_transaction with status paid
    #set status selling as closed, set date_updated as date paid
    #set status approved bidding as paid,set other bidder unapproved to rejected set date_updated as date paid/date rejected
    #insert data addon_reg_certificates
    #insert data addon_reg_green_wallet with current amount saldo
   
  }
}
