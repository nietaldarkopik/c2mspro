<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page_plugin extends MY_Controller {
  
  function __construct()
  {
    /*
    $plugins = $this->plugins->get_plugins();
    $this->plugins->load_plugins();
    if(is_array($this->plugins->plugins))
    {
      foreach($this->plugins->plugins as $i => $p)
      {
        $p->setPath();
      }
    }
    echo "<pre>";
    print_r($this);
    exit;
    * */
    parent::__construct();
  }
  
  function index()
  {
    
  }
  
  
	public function page($block_name="")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
    /*
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_page_block_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_page_block_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_page_block_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_page_block_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_page_block_listing',array($this,'_hook_show_panel_allowed'));
		*/
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('components/default/listing',array('listing_config' => $this->_config(),'response' => '','page_title' => 'Plugin Management','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter));
		else
			$this->load->view('layouts/login');
	}
  
  function fetch_list()
  {
    
  }
  
  function installer()
  {
    
  }
  
  function uninstaller()
  {
    
  }
  
  function updater()
  {
    
  }
  
  function configuration($plugin_id = "")
  {
    if(empty($plugin_id))
      die("Plugin not found");
      
    $plugin = $this->plugins->get_data_plugins("plugin_id = '".$plugin_id."'");
    $plugin = (isset($plugin[0]))?$plugin[0]:$plugin;
    
    if(isset($plugin['plugin_name']))
    {
      redirect(base_url('page_plugin/plugin_controller/'.$plugin['plugin_name']),'reload');
      exit;
    }else{ 
      die("Plugin name undefined");
    }
  }
  
  /*
  public function _remap($method, $params = array())
  {
      $method = 'process_'.$method;
      if (method_exists($this, $method))
      {
          return call_user_func_array(array($this, $method), $params);
      }
      show_404();
  }
  */
  
  function plugin_controller($plugin_name = "",$controller = "",$function = "index")
  {
    #$plugin_controller_class = $plugin_name."controller";
    #$plugin_controller = new $plugin_controller_class();
    #print_r($plugin_controller);
    $segments = $this->uri->segment_array();
    #print_r($segments);
    $this->plugins->set_path($plugin_name);
    $plugin_controller = $this->plugins->call_plugin_controller($plugin_name,$controller);
    $plugin_controller->$function();
  }
  
  function front_controller()
  {
    
  }
  
  function block_controller()
  {
    
  }
  
  function _config()
  {
    $init = array(	'table' => TABLE_SYS_PLUGINS,
                    'fields' => array(
                                  array(
                                    'name' => 'plugin_title',
                                    'label' => 'Plugin Title',
                                    'id' => 'plugin_title',
                                    'value' => '',
                                    'type' => 'input_text',
                                    'use_search' => 'true',
                                    'use_listing' => 'true',
                                    'rules' => 'required'
                                  ),
                                  array(
                                    'name' => 'plugin_name',
                                    'label' => 'Plugin Name',
                                    'id' => 'plugin_name',
                                    'value' => '',
                                    'type' => 'input_text',
                                    'use_search' => 'true',
                                    'use_listing' => 'true',
                                    'rules' => 'required'
                                  ),
                                  array(
                                    'name' => 'status',
                                    'label' => 'Status',
                                    'id' => 'status',
                                    'value' => '',
                                    'type' => 'input_selectbox',
                                    'options' =>  array(
                                                    'active' => 'active',
                                                    'not active' => 'not active'
                                                  ),
                                    'use_search' => 'true',
                                    'use_listing' => 'true',
                                    'rules' => 'required'
                                  ),
                                  array(
                                    'name' => 'description',
                                    'label' => 'Description',
                                    'id' => 'description',
                                    'value' => '',
                                    'type' => 'input_text',
                                    'use_search' => 'true',
                                    'use_listing' => 'true',
                                    'rules' => 'required'
                                  )
                            ),
                            'primary_key' => 'plugin_id',
                            'path' => "/",
                            'controller' => 'page_plugin',
                            'function' => 'page',
                            'panel_function' => array(
                                                        array('title' => 'Install','name' => 'installer', 'class' => 'glyphicon-edit'),
                                                        array('title' => 'Uninstall','name' => 'uninstaller', 'class' => 'glyphicon-share'),
                                                        array('title' => 'Configuration','name' => 'configuration', 'class' => 'glyphicon-cog')
                                                      )
                    );
		$this->init = $init;
    return $init;
  }
  
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
