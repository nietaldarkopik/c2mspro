<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post_categories extends Admin_Controller {

	var $init = array();
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Berita'));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'berita/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('layouts/header');
			$this->load->view('layouts/topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/edit',array('response' => $response,'page_title' => 'Edit Pages'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('layouts/header');
			$this->load->view('layouts/topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Tambah Pages'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('layouts/header');
			$this->load->view('layouts/topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/view',array('response' => '','page_title' => 'Detail Pages'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('layouts/header');
			$this->load->view('layouts/topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Pages'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _config($id_object = "")
	{			
		$init = array(	'table' => 'pages',
						'fields' => array(	
											array(
													'name' => 'judul',
													'label' => 'Judul',
													'id' => 'judul',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'tanggal',
													'label' => 'Tanggal',
													'id' => 'tanggal',
													'value' => '',
													'type' => 'text',
													'class' => 'input_date',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required',
													'list_style' => 'width="150"'
												),
											array(
													'name' => 'content',
													'label' => 'Isi Berita',
													'id' => 'content',
													'value' => '',
													'type' => 'textarea',
													'class' => 'input_tinymce_simple',
													'use_search' => true,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'thumbnail',
													'label' => 'Gambar Thumbnail',
													'id' => 'thumbnail',
													'value' => '',
													'type' => 'file',
													'use_search' => false,
													'use_listing' => true,
													'config_upload' => array('upload_path' => getcwd().'/uploads/media/layouts/','encrypt_name' => true,'allowed_types' =>  'gif|jpg|png'),
													'rules' => '',
													'list_style' => 'width="100" align="center"'
												)
										),
									'primary_key' => 'id'
					);
		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
