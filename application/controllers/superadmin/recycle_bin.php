<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recycle_bin extends Admin_Controller {

	var $init = array();
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('layouts/header');
			$this->load->view('layouts/topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('layouts/default/listing',array('response' => ''));
		else
			$this->load->view('layouts/login');
			
			#$this->load->view('layouts/rightbar');
			$this->load->view('layouts/bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('layouts/footer');
		}
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('layouts/header');
			$this->load->view('layouts/topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/edit',array('response' => $response));
		else
			$this->load->view('layouts/login');
		
			#$this->load->view('layouts/rightbar');
			$this->load->view('layouts/bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('layouts/footer');
		}
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('layouts/header');
			$this->load->view('layouts/topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/add',array('response' => $response));
		else
			$this->load->view('layouts/login');
		
			#$this->load->view('layouts/rightbar');
			$this->load->view('layouts/bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('layouts/footer');
		}
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('layouts/header');
			$this->load->view('layouts/topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/view',array('response' => ''));
		else
			$this->load->view('layouts/login');
		
			#$this->load->view('layouts/rightbar');
			$this->load->view('layouts/bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('layouts/footer');
		}
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('layouts/header');
			$this->load->view('layouts/topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/listing',array('response' => ''));
		else
			$this->load->view('layouts/login');
		
			#$this->load->view('layouts/rightbar');
			$this->load->view('layouts/bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('layouts/footer');
		}
	}
	
	function _config($id_object = "")
	{			
		$init = array(	'table' => 'recycle_bin',
						'fields' => array(	
											array(
													'name' => 'user_name',
													'label' => 'Username',
													'id' => 'user_name',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'password',
													'label' => 'Password',
													'id' => 'password',
													'value' => '',
													'type' => 'password',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'email',
													'label' => 'Email',
													'id' => 'user_name',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required|valid_email'
												),
											array(
													'name' => 'user_level_id',
													'label' => 'User Level',
													'id' => 'user_level_id',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'user_levels',
													'select' => array('user_level_id AS value','user_level_name AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'user_level_id',
													'rules' => 'required'
												),
											array(
													'name' => 'status',
													'label' => 'Status',
													'id' => 'status',
													'value' => '',
													'type' => 'input_selectbox',
													'options' => array('' => '---- Select Option ----','active' => 'Active','not active' => 'Not Active'),
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												)
										),
									'primary_key' => 'user_id'
					);
		$this->init = $init;
	}
	
	function _test()
	{
		$controllers    = array();

        $dir            = APPPATH.'/controllers/';
        $files          = scandir($dir);

        $controller_files = array_filter($files, function($filename) {
            return (substr(strrchr($filename, '.'), 1)=='php') ? true : false;
        });

        foreach ($controller_files as $filename)
        {
            require_once('./application/controllers/'.$filename);

            $classname = ucfirst(substr($filename, 0, strrpos($filename, '.')));
            $controller = new $classname();
            $methods = get_class_methods($controller);

            foreach ($methods as $index => $method)
            {
				if((strpos($method,'_') <= 0 and strpos($method,'_') === 0) or $method == 'get_instance')
				{
					unset($methods[$index]);
				}
            }

            $controller_info = array(
                'filename' => $filename,
                'class_name' => $classname,
                'methods'  => $methods
            );
            array_push($controllers,$controller_info);
        }
	}
	
	function _hook_do_add($param = "")
	{
		$param['password'] = md5($param['password']);
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		if(isset($param['password']) and !empty($param['password']))
			$param['password'] = md5($param['password']);
			
		if(isset($param['password']) and empty($param['password']))
			unset($param['password']);
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
