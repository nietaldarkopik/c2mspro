<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
  
  function __construct()
  {
    parent::__construct();
  }
  
	function index($action = "",$menu_group = "general")
	{
		if($action == "set_menu_group")
		{
			$this->_set_menu_group($menu_group);
		}else{
			$this->session->set_userdata("menu_group",$menu_group);
		}
		
		$is_ajax = $this->input->post('is_ajax');

		$this->load->view('layouts/dashboard');
	}
	
	function mainpage($action = "",$menu_group = "general")
	{
		if($action == "set_menu_group")
		{
			$this->_set_menu_group($menu_group);
		}else{
			$this->session->set_userdata("menu_group",$menu_group);
		}
		
		$is_ajax = $this->input->post('is_ajax');

		$this->load->view('layouts/mainpage');
	}
	
	function login()
	{
		$is_ajax = $this->input->post('is_ajax');
		
		$error= "";
		$user_name = $this->input->post("user_name");
		$password = $this->input->post("password");
		$is_login = $this->user_access->is_login();
		
		if(!empty($user_name) || !empty($password))
		{
			$password = md5($password);
			
			$process_login = $this->user_access->do_login($user_name,$password);
			if($process_login)
			{
				$error = "";
				header("location: " . base_url());
			}else{
				$error = "Username atau password tidak valid";
			}
		}
		
		$this->load->view('layouts/login',array('error' => $error,"page_title" => "Login Page"));
	}
	
	function do_login()
	{
		$user_name = $this->input->post("user_name");
		$password = $this->input->post("password");
		
		if($password == "" and $user_name == "")
		{
			$is_login = $this->user_access->is_login();
			if($is_login)
			{
				$this->load->view('layouts/dashboard');
			}else{
				$this->load->view('layouts/login',array('error' => $error,"page_title" => "Login Page"));
			}
		}else{
			$password = md5($password);
			
			$error= "";
			$process_login = $this->user_access->do_login($user_name,$password);
			if($process_login)
			{
				$error = "";
				$this->load->view('layouts/dashboard');
			}else{
				$error = "Username atau password tidak valid";
				$this->load->view('layouts/login',array('error' => $error,"page_title" => "Login Page"));
			}
		}
	}
	
	function is_login()
	{
			$is_login = $this->user_access->is_login();
			echo $is_login;
	}
	
	function do_logout()
	{
		$this->user_access->do_logout();
		header("location: ".base_url()."dashboard/login");
		exit;
	}
	
	function right_menu()
	{		
		$menus = $this->user_access->get_menus_allowed_structured();
		echo $menus;
	}
	
	function rightbar()
	{		
		$this->load->view('layouts/blocks/rightbar',array('error' => ''));
	}
	
	function top_menu()
	{		
			$menus = $this->user_access->get_menus_allowed_structured("",1);
			echo $menus;
	}
	
	function _set_menu_group($menu_group = "general")
	{
		$user_id = $this->user_access->current_user_id;
		$this->session->set_userdata("menu_group",$menu_group);
		$parent_menu_allowed = $this->user_access->get_menus_allowed($user_id," AND type = '".$menu_group."' AND parent_menu = 0");
		$controller = "dashboard";
		$function = "index";
		if(is_array($parent_menu_allowed) and count($parent_menu_allowed) > 0)
		{
			foreach($parent_menu_allowed as $index => $menu)
			{
				$first_submenu_allowed = $this->user_access->get_menus_allowed($user_id," AND parent_menu = '".$menu['menu_id']."'");
				
				if(is_array($first_submenu_allowed) and count($first_submenu_allowed) > 0)
				{
					foreach($first_submenu_allowed as $i => $m)
					{
						$controller = $m['controller'];
						$function = $m['function'];
						break;
					}
					break;
				}
			}
		}
		
		header('location:' . base_url().$controller.'/'.$function);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
