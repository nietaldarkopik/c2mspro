<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends Admin_Controller {

	var $init = array();
	var $page_title = "Pages";
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('layouts/default/listing',array('response' => '','page_title' => $this->page_title));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'berita/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
    $this->init['action'] = str_replace("add","edit",trim($this->init['action'],'/')).'/'.$object_id;
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('layouts/header');
			$this->load->view('layouts/topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/edit',array('response' => $response,'page_title' => $this->page_title));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/add',array('response' => $response,'page_title' => $this->page_title));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('layouts/header');
			$this->load->view('layouts/topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/view',array('response' => '','page_title' => $this->page_title));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('layouts/default/listing',array('response' => '','page_title' => $this->page_title));
		else
			$this->load->view('layouts/login');
			
	}
	
	function _config($id_object = "")
	{			
		$init = array(	'table' => 'user_pages',
						'fields' => array(
											array(
													'name' => 'page_title',
													'label' => 'Page Title',
													'id' => 'page_title',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required',
													'list_style' => 'width="150"'
												),
											array(
													'name' => 'page_name',
													'label' => 'Page URL',
													'id' => 'page_name',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'page_description',
													'label' => 'Page Description',
													'id' => 'page_description',
													'value' => '',
													'type' => 'textarea',
													'use_search' => true,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'author',
													'label' => 'Author',
													'id' => 'author',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'date_inserted',
													'label' => 'Date Inserted',
													'id' => 'date_inserted',
													'value' => '',
													'type' => 'input_datetime',
													'use_search' => true,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'date_updated',
													'label' => 'Date Updated',
													'id' => 'date_updated',
													'value' => '',
													'type' => 'input_datetime',
													'use_search' => true,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'status',
													'label' => 'Status',
													'id' => 'status',
													'value' => '',
													'type' => 'input_text',
													'use_search' => true,
													'use_listing' => false,
													'rules' => 'required'
												)
										),
									'primary_key' => 'user_page_id',
                  'action' => base_url('admin/layouts/add'),
                  'action_search' => base_url('admin/layouts/listing'),
					);
		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
