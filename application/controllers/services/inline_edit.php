<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inline_edit extends Admin_Controller {

	var $init = array();
	var $page_title = "";
	
	function index()
	{
			
	}
	
	
	function save_page($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$data = array(	"page_description" => html_entity_decode($this->input->post("page_description")),
						"status" => "active"
						);
		$_POST['data'] = $data;
		$user_level = $this->CI->user_access->get_level();
		if($user_level == '1')
		{
			$response = $this->data->edit("",$this->init['fields']);
			$response = (empty($this->data->errors))?$response:$this->data->errors;
			echo strip_tags($response);
		}else{
			echo "Permission denied.";
		}
	}
	
	function _config($id_object = "")
	{
    $init = array(	
            'table' => "plugin_contents",
						'fields' => array(
										array(
											'name' => 'page_url',
											'label' => 'Page URL',
											'id' => 'page_url',
											'value' => '',
											'type' => 'input_hidden',
											'use_search' => false,
											'use_listing' => true,
											'rules' => '',
											'readonly' => ''
										),
										array(
											'name' => 'page_title',
											'label' => 'Title',
											'id' => 'page_title',
											'value' => '',
											'type' => 'input_hidden',
											'use_search' => true,
											'use_listing' => true,
											'rules' => ''
										),
										array(
											'name' => 'page_description',
											'label' => 'Content',
											'id' => 'page_description',
											'value' => '',
											'type' => 'input_selectbox',
											'use_search' => false,
											'use_listing' => true,
											'rules' => ''
										),
										array(
											'name' => 'position',
											'label' => 'Position',
											'id' => 'position',
											'value' => '',
											'type' => 'input_selectbox',
											'options' => array('not active' => 'Not Active','active' => 'Active'),
											'use_search' => false,
											'use_listing' => true,
											'rules' => ''
										),
										array(
											'name' => 'status',
											'label' => 'Status',
											'id' => 'status',
											'value' => '',
											'type' => 'input_selectbox',
											'options' => array('not active' => 'Not Active','active' => 'Active'),
											'use_search' => false,
											'use_listing' => true,
											'rules' => ''
										)
									),
						'path' => "/admin/",
						'controller' => 'inline_edit',
						'function' => 'index',
						'primary_key' => 'page_id',
						'panel_function' => array(
													array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
												  )
			);
		$this->init = $init;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
