<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Plugin_image_sliders extends Admin_Controller {

	var $init = array();
	var $page_title = "";
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/admin/_plugin_image_sliders_image_list',array($this,'_hook_show_panel_allowed_image_list'));
		
		$is_login = $this->user_access->is_login();

		$config_form_filter = $this->init;
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
		$config_form_add = $this->init;
		$config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/image_sliders/listing',array('response' => '','page_title' => 'Data Sliders','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function image_list($slider_id = "")
	{
		$this->_config_image_list();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_image_url',array($this,'_hook_create_listing_value_image_list'));
		
		$is_login = $this->user_access->is_login();

		$config_form_filter = $this->init;
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2)).'/image_list';
		$config_form_add = $this->init;
		$config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add_image_list');
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add_image_list');
		if($is_login)
			$this->load->view('layouts/image_sliders/listing_image_list',array('response' => '','page_title' => 'Data Image List','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete_image_list($object_id = "")
	{
		$this->_config_image_list();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$the_data = $this->data->data_rows;
		
		$the_data = (isset($the_data[0]))?$the_data[0]:$the_data;
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		if(isset($the_data['plugin_slider_id']))
		{
			$this->image_list($the_data['plugin_slider_id']);
		}else{
			$this->listing();
		}
	}	
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_listing',array($this,'_hook_show_panel_allowed'));

		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/image_sliders/edit',array('response' => $response,'page_title' => 'Data Sliders'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function add_image_list()
	{
		$this->_config_image_list();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add_image_list'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/image_sliders/add',array('response' => $response,'page_title' => 'Data Sliders'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function view_image_list($object_id = "")
	{
		$this->_config_config_image_list();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_listing',array($this,'_hook_show_panel_allowed'));

		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/image_sliders/view',array('response' => '','page_title' => 'Data Sliders'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing_image_list()
	{
		$this->_config_image_list();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_image_url',array($this,'_hook_create_listing_value_image_list'));
		
		$is_login = $this->user_access->is_login();


    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2)).'/image_list';
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add_image_list');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add_image_list');
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/image_sliders/listing_image_list',array('response' => '','page_title' => 'Data Image List','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function edit_image_list($object_id = "")
	{
		$this->_config_image_list();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit_image_list'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_listing',array($this,'_hook_show_panel_allowed'));

		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'image_url')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/image_sliders/edit_image_list',array('response' => $response,'page_title' => 'Data Sliders'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/image_sliders/add',array('response' => $response,'page_title' => 'Data Sliders'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_listing',array($this,'_hook_show_panel_allowed'));

		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/image_sliders/view',array('response' => '','page_title' => 'Data Sliders'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_image_sliders_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/admin/_plugin_image_sliders_image_list',array($this,'_hook_show_panel_allowed_image_list'));
		
		$is_login = $this->user_access->is_login();


    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/image_sliders/listing',array('response' => '','page_title' => 'Data Sliders','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function page(){}
	function plugin_list(){}
	function plugin_configuration(){}
	function plugin_page_visibility(){}

	function _config($id_object = "")
	{
    $init = array(	'table' => "plugin_sliders",
						'fields' => array(
										array(
											'name' => 'title',
											'label' => 'Title',
											'id' => 'title',
											'value' => '',
											'type' => 'input_text',
											'use_search' => true,
											'use_listing' => true,
											'rules' => 'required'
										),
										array(
											'name' => 'description',
											'label' => 'Description',
											'id' => 'description',
											'value' => '',
											'type' => 'input_text',
											'use_search' => true,
											'use_listing' => true,
											'rules' => 'required'
										)
									),
									'path' => "/admin/",
									'controller' => 'plugin_image_sliders',
									'function' => 'index',
									'primary_key' => 'plugin_slider_id',
									'panel_function' => 
											array(
                                                array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-cog'),
                                                array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog'),
                                                array('title' => 'Image Lists','name' => 'image_list', 'class' => 'glyphicon-cog')
                                              )
          );
		$this->init = $init;
	}
	
	function _config_image_list($id_object = "")
	{
    $init = array(	'table' => "plugin_slider_images",
						'fields' => array(
										array(
											'name' => 'plugin_slider_id',
											'label' => 'Slider',
											'id' => 'plugin_slider_id',
											'value' => $this->uri->segment(4),
											'type' => 'input_hidden',
											'use_search' => false,
											'use_listing' => false,
											'rules' => ''
										),
										array(
											'name' => 'title',
											'label' => 'Title',
											'id' => 'title',
											'value' => '',
											'type' => 'input_text',
											'use_search' => true,
											'use_listing' => true,
											'rules' => 'required'
										),
										array(
											'name' => 'text1',
											'label' => 'Text 1',
											'id' => 'text1',
											'value' => '',
											'type' => 'input_text',
											'use_search' => true,
											'use_listing' => true,
											'rules' => ''
										),
										array(
											'name' => 'text2',
											'label' => 'Text 2',
											'id' => 'text2',
											'value' => '',
											'type' => 'input_text',
											'use_search' => true,
											'use_listing' => true,
											'rules' => ''
										),
										array(
											'name' => 'image_url',
											'label' => 'Image',
											'id' => 'image_url',
											'value' => '',
											'type' => 'input_file',
											'use_search' => false,
											'use_listing' => true,
											'rules' => 'required',
											'config_upload' => array( 
																	'upload_path' => dirname($_SERVER['SCRIPT_FILENAME']).'/uploads/media/image_sliders/',
																	'encrypt_name' => false,
																	'allowed_types' =>  'jpg|png|gif'
																	),
											'rules' => 'required_file|required',
											'list_style' => 'width="200"'
										),
										array(
											'name' => 'link',
											'label' => 'Link Url',
											'id' => 'link',
											'value' => '',
											'type' => 'input_text',
											'use_search' => true,
											'use_listing' => true,
											'rules' => ''
										)
									),
									'path' => "/admin/",
									'controller' => 'plugin_image_sliders',
									'function' => 'image_list',
									'primary_key' => 'plugin_slider_image_id',
									'panel_function' => 
											array(
                                                array('title' => 'Edit','name' => 'edit_image_list', 'class' => 'glyphicon-cog'),
                                                array('title' => 'Delete','name' => 'delete_image_list', 'class' => 'glyphicon-cog')
                                              )
          );
		$this->init = $init;
	}
	
	function _hook_do_add_image_list($param = "")
	{
		#$param['plugin_slider_id'] = $this->uri->segment(3);
		if(empty($param['link']))
			$param['link'] = "#";
			
		print_r($param);
		return $param;
	}
	
	function _hook_do_edit_image_list($param = "")
	{
		if(isset($param['plugin_slider_id']))
			unset($param['plugin_slider_id']);
		if(empty($param['link']))
			$param['link'] = "#";
		return $param;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Add New Slider";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Slider";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
  
  function _hook_show_panel_allowed_image_list($panel = "")
  {
	$panel = str_replace(" ajax\"","\"",$panel);
    return $panel;
  }
  
  function _hook_create_listing_value_image_list($value = "")
  {
	$value = str_replace("height=\"50\"","width=\"200\"",$value);
	$value = str_replace("<td>","<td width=\"200\"",$value);
    return $value;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
