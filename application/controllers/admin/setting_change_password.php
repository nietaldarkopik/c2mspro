<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting_change_password extends Admin_Controller {

	var $init = array();
	var $page_title = "";
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();

		$config_form_filter = $this->init;
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
		$config_form_add = $this->init;
		$config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/setting_change_password/listing',array('response' => '','page_title' => 'Data Widget','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function widgets()
	{
		$this->_config_widgets();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/setting_change_password/listing',array('response' => '','page_title' => 'Data Widget','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_listing',array($this,'_hook_show_panel_allowed'));

		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/setting_change_password/edit',array('response' => $response,'page_title' => 'Data Widget'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/setting_change_password/add',array('response' => $response,'page_title' => 'Data Widget'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_listing',array($this,'_hook_show_panel_allowed'));

		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/setting_change_password/view',array('response' => '','page_title' => 'Data Widget'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_setting_change_password_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();


    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/setting_change_password/listing',array('response' => '','page_title' => 'Data Widget','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function page(){}
	function plugin_list(){}
	function plugin_configuration(){}
	function plugin_page_visibility(){}

	function _config($id_object = "")
	{
    $init = array(	'table' => TABLE_SYS_BLOCKS,
						'fields' => array(
													array(
														'name' => 'block_name',
														'label' => 'Block Name',
														'id' => 'block_name',
														'value' => '',
														'type' => 'input_text',
														'use_search' => true,
														'use_listing' => true,
														'rules' => 'required'
													),
													array(
														'name' => 'block_title',
														'label' => 'Block Title',
														'id' => 'block_title',
														'value' => '',
														'type' => 'input_text',
														'use_search' => true,
														'use_listing' => true,
														'rules' => 'required'
													)
                    ),
                    'path' => "/admin/",
                    'controller' => 'setting_change_password',
                    'function' => 'index',
                    'primary_key' => 'block_id',
                    'panel_function' => array(
                                                array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
                                              )
          );
		$this->init = $init;
	}
	
	function _config_widgets($id_object = "")
	{
    $init = array(	
            //'table' => TABLE_SYS_BLOCKS,
            'query' => "SELECT p.*,b.* FROM ".TABLE_SYS_BLOCKS." b,".TABLE_SYS_BLOCK_PLUGINS." bp ,".TABLE_SYS_PLUGINS." p ",
            'where' => " b.block_id = bp.block_id AND p.plugin_id = bp.plugin_id ",
						'fields' => array(
													array(
														'name' => 'plugin_title',
														'label' => 'Widget Title',
														'id' => 'plugin_title',
														'value' => '',
														'type' => 'input_text',
														'use_search' => true,
														'use_listing' => true,
														'rules' => 'required'
													),
													array(
														'name' => 'plugin_name',
														'label' => 'Widget Name',
														'id' => 'plugin_name',
														'value' => '',
														'type' => 'input_text',
														'use_search' => true,
														'use_listing' => true,
														'rules' => 'required'
													),
													array(
														'name' => 'block_name',
														'label' => 'Block Name',
														'id' => 'block_name',
														'value' => '',
														'type' => 'input_text',
														'use_search' => false,
														'use_listing' => false,
														'rules' => 'required'
													),
													array(
														'name' => 'block_title',
														'label' => 'Display on Block',
														'id' => 'block_title',
														'value' => '',
														'type' => 'input_text',
														'use_search' => true,
														'use_listing' => true,
														'rules' => 'required'
													),
													array(
														'name' => 'status',
														'label' => 'Status',
														'id' => 'status',
														'value' => '',
														'type' => 'input_text',
														'use_search' => true,
														'use_listing' => true,
														'rules' => 'required'
													),
													array(
														'name' => 'position',
														'label' => 'Order Position',
														'id' => 'position',
														'value' => '',
														'type' => 'input_text',
														'use_search' => true,
														'use_listing' => true,
														'rules' => 'required'
													)
                    ),
                    'primary_key' => 'block_id',
                    'path' => "/",
                    'controller' => 'setting_change_password',
                    'function' => 'widgets',
                    'panel_function' => array(
                                                array('title' => 'Install','name' => 'add', 'class' => 'glyphicon-edit'),
                                                array('title' => 'Uninstall','name' => 'delete', 'class' => 'glyphicon-share'),
                                                array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
                                              ),
                    'bulk_options' => array(
                                                array('title' => 'Install','name' => 'add', 'class' => 'glyphicon-edit'),
                                                array('title' => 'Uninstall','name' => 'delete', 'class' => 'glyphicon-share'),
                                                array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
                                              )
          );
		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "ADD NEW WIDGET";
  }
  
  function _hook_create_form_title_edit($title){
    return "EDIT WIDGET";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
