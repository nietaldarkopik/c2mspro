<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class MY_Controller extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $is_login = $this->user_access->is_login();

        /*if(!$is_login and $this->uri->segment(1) != "dashboard" and $this->uri->segment(2) != "login")
        {
          header("location: ".base_url()."dashboard/login");
        }*/
        #$this->load->add_package_path(FCPATH.'packages/themes/user',true);
        #$this->load->_ci_view_paths = array(FCPATH.'themes/user/'.CURRENT_THEME.'/templates/'	=> TRUE);
        $this->load->set_object_path('view',array(FCPATH.'themes/user/'.CURRENT_THEME.'/templates/'	=> TRUE));
        
        if(file_exists(FCPATH.'themes/user/'.CURRENT_THEME.'/'.CURRENT_THEME . '.php'))
        {
          require_once(FCPATH.'themes/user/'.CURRENT_THEME.'/'.CURRENT_THEME . '.php');
          $theme_config = CURRENT_THEME;
          $theme_loader = new $theme_config();
          $theme_loader->setAssets();
        }
        
        $user_id = $this->user_access->current_user_id;
        $function = $this->uri->segment(3,"index");
        $controller = $this->uri->segment(2,$function);
        $path = $this->uri->segment(1,$controller);
        
        $user_level_name = $this->user_access->get_level_detail($this->session->userdata("user_id"));
        $user_level_name = (isset($user_level_name['user_level_name']))?$user_level_name['user_level_name'].'/':"";
        
        $path = trim($path,"/");
        $path = "/".$path."/";
        $is_page_allowed = $this->user_access->is_page_allowed($this->session->userdata("user_id"),$path,$controller,$function);
        
        $user_id = $this->user_access->current_user_id;
        $controller = $this->uri->segment(1);
        $function = $this->uri->segment(2,"index");
        
        if(!$is_login and $this->uri->segment(3) != "login" and $this->uri->segment(3) != "do_login")
        {
          if($this->uri->segment(1) != "dashboard" and $this->uri->segment(1) != "")
          {
            if(!$this->user_access->is_page_allowed($user_id,$controller,$function))
            {
              redirect(base_url()."dashboard/login");
              #show_error('Anda tidak memiliki ijin untuk mengakses halaman ini',500,'404 Page Not Found');
            }
          }
        }
        
        if(ENVIRONMENT == 'production')
        {
          $this->output->cache(5);
        }
		
    }
}

class Admin_Controller extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $is_login = $this->user_access->is_login();
        if(!$is_login and $this->uri->segment(1) != "admin" and $this->uri->segment(2) != "dashboard" and $this->uri->segment(3) != "login")
        {
          header("location: ".base_url()."admin/dashboard/login");
        }
        #$this->load->add_package_path(FCPATH.'packages/themes/admin',true);
        #$this->_ci_view_paths = array(FCPATH.'themes/admin/'.CURRENT_ADMIN_THEME.'/templates/'	=> TRUE);
        $this->load->set_object_path('view',array(FCPATH.'themes/admin/'.CURRENT_ADMIN_THEME.'/templates/'	=> TRUE),TRUE);
        $this->assets->reset();
        if(file_exists(FCPATH.'themes/admin/'.CURRENT_ADMIN_THEME.'/'.CURRENT_ADMIN_THEME . '.php'))
        {
          require_once(FCPATH.'themes/admin/'.CURRENT_ADMIN_THEME.'/'.CURRENT_ADMIN_THEME . '.php');
          $theme_config = CURRENT_ADMIN_THEME;
          $theme_loader = new $theme_config();
          $theme_loader->setAssets();
        }

        $user_id = $this->user_access->current_user_id;
        $controller = $this->uri->segment(1);
        $function = $this->uri->segment(2,"index");
        
        if(!$is_login and $this->uri->segment(3) != "login" and $this->uri->segment(3) != "do_login")
        {
          if($this->uri->segment(1) != "dashboard" and $this->uri->segment(1) != "")
          {
            if(!$this->user_access->is_page_allowed($user_id,$controller,$function))
            {
              redirect(base_url()."admin/dashboard/login");
              #show_error('Anda tidak memiliki ijin untuk mengakses halaman ini',500,'404 Page Not Found');
            }
          }
        }
        if(ENVIRONMENT == 'production')
        {
          $this->output->cache(5);
        }
        
    }
      
    function add_csv()
    {
      $this->_config();
      $this->data->init($this->init);
      $this->data->set_filter();
      $this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
      $data = $this->input->post("data");
      $fields = $this->init['fields'];
      
      $response = $this->data->add($data,$fields);

      $error = "";
      if(is_array($fields) and count($fields) > 0)
      {
        foreach($data as $i => $field)
        {
          $name = (isset($field['name']))?$field['name']:"";
          $parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
          $input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
          $error .= form_error($input_name);
        }
      }
      
      $response .= $error;
      $is_login = $this->user_access->is_login();
      
      if($is_login)			
        echo $response;
    }
    
      
    function check_csv()
    {
      $this->_config();
      $this->data->init($this->init);
      $this->data->set_filter();

      $response = $this->data->check_csv("",$this->init['fields']);
      #$response = str_replace("\n","",$response);
      #$response = str_replace("\"","\\\"",$response);
      //$response = strip_tags($response);
      #$response = addslashes($response);
      echo $response;
    }
}

class Plugins_Controller extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->setPath();
	}
	
	function setPath()
	{
		$this->load->set_object_path('library',array(FCPATH.'plugins/'	=> TRUE),false);
		$this->load->set_object_path('helper',array(FCPATH.'plugins/'	=> TRUE),false);
		$this->load->set_object_path('model',array(FCPATH.'plugins/'	=> TRUE),false);
		$this->load->set_object_path('view',array(FCPATH.'plugins/'	=> TRUE),false);
		#$this->assets->reset();
	}
}

// END Controller class

/* End of file Controller.php */
/* Location: ./system/core/Controller.php */
