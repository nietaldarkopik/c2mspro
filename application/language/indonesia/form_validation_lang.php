<?php
$lang['required']                       = "Data %s diperlukan.";
$lang['isset']                          = "Data %s harus diisi.";
$lang['valid_email']				            = "Data %s harus berisikan alamat email yang benar.";
$lang['valid_emails']       				    = "Data %s harus berisikan alamat email yang benar.";
$lang['valid_url']                      = "Data %s harus berisikan URL yang benar.";
$lang['valid_ip']                       = "Data %s harus berisikan IP yang benar.";
$lang['min_length']                     = "Data %s panjang minimal %s karakter.";
$lang['max_length']                     = "Data %s panjang maksimal %s karakter.";
$lang['exact_length']						        = "Data %s harus berisikan %s karakter.";
$lang['alpha']                          = "Data %s hanya dibolehkan berisikan alphabetical karakter.";
$lang['alpha_numeric']				          = "Data %s hanya dibolehkan berisikan alpha-numeric karakter.";
$lang['alpha_dash']                     = "Data %s hanya dibolehkan berisikan alpha-numeric karakter, underscores, dan dashes.";
$lang['numeric']                        = "Data %s harus berisikan angka.";
$lang['is_numeric']                     = "Data %s harus berisikan numeric karakter.";
$lang['integer']                        = "Data %s harus berisikan integer.";
$lang['regex_match']          				  = "Data %s tidak berisikan format yang benar.";
$lang['matches']                        = "Data %s tidak cocok dengan %s.";
$lang['is_natural']                     = "Data %s harus berisikan angka positif.";
$lang['is_natural_no_nol']    					= "Data %s harus berisikan angka lebih besar dari nol.";
$lang['decimal']                        = "Data %s harus berisikan angka desimal.";
$lang['less_than']                      = "Data %s harus berisikan angka lebih kecil dari %s.";
$lang['greater_than']          					= "Data %s harus berisikan angka lebih besar dari %s.";
$lang['is_unique']          						= "Data %s harus unik (tidak boleh ada yang sama).";
$lang['is_count_match']          				= "Jumlah kolom data harus berjumlah %s data.";
$lang['valid_date']           			  	= "Data %s harus berisikan tanggal exp. ".date("Y-m-d");
