<?php
$lang['upload_userfile_not_set']        = "Tidak dapat menemukan variabel pos userfile.";
$lang['upload_file_exceeds_limit']      = "Berkas yang diunggah melebihi ukuran maksimum yang diperbolehkan dalam file konfigurasi PHP Anda.";
$lang['upload_file_exceeds_form_limit'] = "Berkas yang diunggah melebihi ukuran maksimum yang diperbolehkan oleh formulir pengiriman.";
$lang['upload_file_partial']            = "Berkas tersebut hanya berhasil diunggah sebagian.";
$lang['upload_no_temp_directory']       = "Folder sementara hilang.";
$lang['upload_unable_to_write_file']    = "File tidak dapat dituliskan ke media penyimpanan.";
$lang['upload_stopped_by_extension']    = "Upload File dihentikan oleh ekstensi.";
$lang['upload_no_file_selected']        = "Anda tidak memilih file untuk diupload.";
$lang['upload_invalid_filetype']        = "Jenis File yang Anda coba upload tidak diperbolehkan.";
$lang['upload_invalid_filesize']        = "File yang Anda coba untuk diupload lebih besar dari ukuran yang diizinkan.";
$lang['upload_invalid_dimensions']      = "Gambar yang sedang Anda upload melebihi ketinggian atau lebar maksimum.";
$lang['upload_destination_error']       = "Terjadi masalah ketika mencoba untuk memindahkan file upload ke tujuan akhir.";
$lang['upload_no_filepath']             = "Tujuan upload tampaknya tidak tersedia.";
$lang['upload_no_file_types']           = "Anda belum menentukan jenis file yang diperbolehkan.";
$lang['upload_bad_filename']            = "Nama file yang Anda kirimkan sudah ada di server.";
$lang['upload_not_writable']            = "Folder tujuan pengunggahan tampaknya tidak dapat ditulis.";
