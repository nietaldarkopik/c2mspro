<?php
class M_component extends CI_Model {
    
  function pagination($config = array(),$total_data = 0)
  {
		$this->load->library("pagination");
		$paging_config = array();
		$paging_config['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'/page/';
		$paging_config['total_rows'] = $total_data;
		$paging_config['per_page'] = 10; 
		$paging_config['uri_segment'] = 3;
		$paging_config['num_links'] = 2;
		$paging_config['use_page_numbers'] = FALSE;
		$paging_config['page_query_string'] = FALSE;
		$paging_config['full_tag_open'] = '<ul class="pagination custom" id="pager">';
		$paging_config['full_tag_close'] = '</ul>';
		$paging_config['first_link'] = '&laquo;';
		$paging_config['first_tag_open'] = '<li>';
		$paging_config['first_tag_close'] = '</li>';
		$paging_config['last_link'] = '&raquo;';
		$paging_config['last_tag_open'] = '<li>';
		$paging_config['last_tag_close'] = '</li>';
		$paging_config['next_link'] = '&rsaquo;';
		$paging_config['next_tag_open'] = '<li>';
		$paging_config['next_tag_close'] = '</li>';
		$paging_config['prev_link'] = '&lsaquo;';
		$paging_config['prev_tag_open'] = '<li>';
		$paging_config['prev_tag_close'] = '</li>';
		$paging_config['cur_tag_open'] = '<li class="active""><a href="#">';
		$paging_config['cur_tag_close'] = '</a></li>';
		$paging_config['num_tag_open'] = '<li>';
		$paging_config['num_tag_close'] = '</li>';
    
    $config = array_merge($paging_config,$config);
		$this->pagination->initialize($paging_config); 
		$output =  $this->pagination->create_links();
    $output =   ' <div class="bottom-page-navi">
                    <div class="row">
                      <div class="col-md-12">
                      '.$output.'
                      </div>
                    </div>
                  </div>';
    return $output;
  }
}
