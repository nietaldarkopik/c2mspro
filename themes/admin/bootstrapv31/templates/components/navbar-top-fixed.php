<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  if($is_ajax == 1){}else{
		$is_login = $this->user_access->is_login();
    if($is_login)
    {
?>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	  <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="<?php echo base_url();?>admin/"><?php echo WEB_NAME;?></a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <?php
			#$this->load->view("components/topmenu");
		  ?>
		  
		  <ul class="nav navbar-nav">
			<li>
			  <a data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/adminthemes" title="Templates Management">Templates</a>
			</li>	
			<li>
			  <a data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/adminblocks" title="Widget Management">Widget</a>
			</li>
			<!--
			<li>
			  <a data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/adminplugins" title="Plugins Management">Plugins</a>
			</li>
			-->
			<li class="dropdown">
			  <a href="#" title="Plugins Management" class="dropdown-toggle" data-toggle="dropdown">Plugins <i class="glyphicon glyphicon-angle-down"></i></a>
			  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
				<li>
				  <a data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/plugin_image_sliders" title="Image Sliders">Image Sliders</a>
				</li>
				<li>
				  <a data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/plugin_products" title="Product Manager">Product Manager</a>
				</li>
				<li>
				  <a data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/plugin_messages" title="Messages">Messages</a>
				</li>
			  </ul>
			</li>	
			<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Setting <i class="glyphicon glyphicon-angle-down"></i></a>
			  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
				<li>
				  <a data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/setting_general" title="General">General</a>
				</li>
				<li>
				  <a data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/setting_change_password" title="Change Password">Change Password</a>
				</li>
				<li>
				  <a data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/setting_inline_edit" title="Inline Edit Mode">Inline Edit Mode</a>
				</li>
			  </ul>
			</li>
			<li>
			  <a href="<?php echo base_url();?>admin/dashboard/do_logout" title="Logout">Logout</a>
			</li>	
		  </ul>
		  <!--
		  <form class="navbar-form navbar-right" role="search">
			<div class="form-group">
			  <input type="text" class="form-control" placeholder="Search">
			</div>
			<button type="submit" class="btn btn-default">Submit</button>
		  </form>	
		  -->
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
  <!-- Modal -->
  <div class="modal fade" id="popupoverlay" tabindex="-1" role="dialog" aria-labelledby="overlaycontainer" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content ajax_container" id="data_target-modal">
      </div>
    </div>
  </div>
<?php 
  }
} ?>
