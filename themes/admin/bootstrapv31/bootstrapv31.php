<?php

class bootstrapv31{
  var $CI;
  
  function __construct()
  {
    $this->CI =& get_instance();
  }
  
  function setAssets()
  {
	  
        $css = '
	<link rel="stylesheet" href="'.current_admin_theme_url().'assets/fonts/stylesheet.css" type="text/css" />
	<link rel="stylesheet" href="'.current_admin_theme_url().'assets/plugins/bootstrap/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="'.current_admin_theme_url().'dist/bootstrap-3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="'.current_admin_theme_url().'dist/bootstrap-3.3.2/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="'.current_admin_theme_url().'assets/plugins/bxslider/jquery.bxslider.css" type="text/css" />
	<link rel="stylesheet" href="'.current_admin_theme_url().'assets/plugins/fancybox/jquery.fancybox.css" type="text/css" />
	<link rel="stylesheet" href="'.current_admin_theme_url().'assets/scripts/jquery/css/ui-darkblue/jquery-ui-1.10.3.custom.min.css" type="text/css" />
	<link rel="stylesheet" href="'.current_admin_theme_url().'assets/css/style.css" type="text/css" />
	<link rel="stylesheet" href="'.current_admin_theme_url().'assets/css/custom.css" type="text/css" />
	<link rel="stylesheet" href="'.current_admin_theme_url().'assets/css/sb-admin-2.css" type="text/css" />
	<link rel="stylesheet" href="'.current_admin_theme_url().'assets/css/bootstrap-theme.css">
	<link rel="stylesheet" href="'.current_admin_theme_url().'assets/css/bootstrap-theme-admin.css">
	<link rel="stylesheet" href="'.current_admin_theme_url().'assets/jquery/jquery-ui-1.11.2/jquery-ui.min.css">
	<link rel="stylesheet" href="'.current_admin_theme_url().'assets/jquery/jquery-ui-1.11.2/jquery-ui.structure.min.css">
	<link rel="stylesheet" href="'.current_admin_theme_url().'assets/jquery/jquery-ui-1.11.2/jquery-ui.theme.css">
              ';        
        $js_head = '
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/jquery/jquery-1.10.2/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/jquery/jquery-ui-1.11.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="'.current_admin_theme_url().'dist/bootstrap-3.3.2/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/jquery/tinymce_1.4/tinymce.min.js"></script>
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/jquery/tinymce_1.4/jquery.tinymce.min.js"></script>
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/js/jquery.form.js"></script>
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/js/ajax.js"></script>
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/js/data.js"></script>
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/js/blocks.js"></script>
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/js/script.js"></script>		

	<!-- <script type="text/javascript" src="'.current_admin_theme_url().'assets/plugins/jquery-1.10.1.min.js"></script>
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/scripts/jquery/js/jquery-ui-1.10.3.custom.js"></script>
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/plugins/bootstrap/js/bootstrap.min.js"></script> -->
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/plugins/hover-dropdown.js"></script>
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/plugins/jquery.migrate.min.js"></script>

	<script type="text/javascript" src="'.current_admin_theme_url().'assets/plugins/modernizr.js"></script>
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/plugins/jquery.easing.min.js"></script>
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/plugins/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/plugins/fancybox/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/scripts/holder.js"></script>
	<script type="text/javascript" src="'.current_admin_theme_url().'assets/plugins/back-to-top.js"></script>

	<!--[if lt IE 9]>
	<script src="'.current_admin_theme_url().'assets/plugins/respond.min.js"></script>  
	<![endif]-->
	<!-- CORE PLUGINS END -->

	<script type="text/javascript" src="'.current_admin_theme_url().'assets/plugins/bxslider/jquery.bxslider.min.js"></script>';
				
		$js_body = '
				<script type="text/javascript">
				  $(function () {
					$(".bxslider").bxSlider({
					  minSlides: 1,
					  maxSlides: 3,
					  slideWidth: 165,
					  slideMargin: 15,
					  controls: true                     // true, false - previous and next controls
					});
					$("#mangstab").tab("show");
					var hash = document.location.hash;
					var prefix = "tab_";
					if (hash) {
						$(".nav-tabs a[href="+hash.replace(prefix,"")+"]").tab("show");
					} 

					$(".nav-tabs a").on("shown", function (e) {
						window.location.hash = e.target.hash.replace("#", "#" + prefix);
					});
					window.scrollTo(0,0);
					
					$("a[rel=catalogue]").fancybox({
					  "transitionIn"		: "fade",
					  "transitionOut"		: "fade",
					  "padding"		      : "0",
					  "titlePosition" 	: "outside",
					  "titleFormat"		: function(title, currentArray, currentIndex, currentOpts) {
						return \'<span id="fancybox-title-over">Image \' + (currentIndex + 1) + \' / \' + currentArray.length + (title.length ? \' &nbsp; \' + title : \'\') + \'</span>\';
					  }
					});
				  });
				  var head = document.getElementsByTagName("head")[0],
					  style = document.createElement("style");
				  style.type = "text/css";
				  if (style.styleSheet)
				  {
					style.styleSheet.cssText = ":before,:after{content:none !important;}";
				  } else {
					style.appendChild(document.createTextNode(":before,:after{content:none !important;}"));
				  }
				  head.appendChild(style);
				  setTimeout(function(){
					  head.removeChild(style);
				  }, 0);
				</script>';

		$this->CI->assets->add_css($css,"head");
        $this->CI->assets->add_js($js_head,"head");
		$this->CI->assets->add_js($js_body,"body");
	}
}
