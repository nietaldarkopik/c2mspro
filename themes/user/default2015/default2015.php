<?php

class default2015{
  var $CI;
  
  function __construct()
  {
    $this->CI =& get_instance();
  }
  
  function setAssets()
  {
    $css = '
            <link rel="stylesheet" href="'.current_theme_url().'assets/bootstrap-3.2.0/css/bootstrap.min.css">
            <link rel="stylesheet" href="'.current_theme_url().'assets/bootstrap-3.2.0/css/bootstrap-theme.min.css">
            <link rel="stylesheet" href="'.current_theme_url().'assets/jquery/jquery-ui-1.11.2/jquery-ui.min.css">
            <link rel="stylesheet" href="'.current_theme_url().'assets/jquery/jquery-ui-1.11.2/jquery-ui.structure.min.css">
            <link rel="stylesheet" href="'.current_theme_url().'assets/jquery/jquery-ui-1.11.2/jquery-ui.theme.css">
            <link rel="stylesheet" href="'.current_theme_url().'assets/css/bootstrap-theme.css">
            <link rel="stylesheet" href="'.current_theme_url().'assets/css/bootstrap-theme-admin.css">';
    $this->CI->assets->add_css($css,"head");
    
    $js = '
            <script type="text/javascript" src="'.current_theme_url().'assets/jquery/jquery-1.10.2/jquery.js"></script>
            <script type="text/javascript" src="'.current_theme_url().'assets/bootstrap-3.2.0/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="'.current_theme_url().'assets/jquery/jquery-ui-1.11.2/jquery-ui.min.js"></script>
            <script type="text/javascript" src="'.current_theme_url().'assets/jquery/tinymce_1.4/tinymce.min.js"></script>
            <script type="text/javascript" src="'.current_theme_url().'assets/jquery/tinymce_1.4/jquery.tinymce.min.js"></script>
            <script type="text/javascript" src="'.current_theme_url().'assets/js/jquery.form.js"></script>
            <script type="text/javascript" src="'.current_theme_url().'assets/js/ajax.js"></script>
            <script type="text/javascript" src="'.current_theme_url().'assets/js/data.js"></script>
            <script type="text/javascript" src="'.current_theme_url().'assets/js/blocks.js"></script>
            <script type="text/javascript" src="'.current_theme_url().'assets/js/script.js"></script>
            <script type="text/javascript">
                $(\'.carousel\').carousel()
            </script>';
    $this->CI->assets->add_js($js,"body");
  }
}
