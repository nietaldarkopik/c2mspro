<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
  <?php 
    $notifications = $this->gts_notification->get_all_notification(); 
    $notif_unreads = $this->gts_notification->get_notification_unread();
  ?>
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      <i class="glyphicon glyphicon-globe"></i>
      <?php
        if(is_array($notif_unreads) and count($notif_unreads) > 0){
      ?>
        <span class="badge"><?php echo count($notif_unreads);?></span>
      <?php
      }
      ?>
    </a>
    <ul class="dropdown-menu">
      <li class="dropdown-header">&nbsp;</li>
      <?php
        $container_status = "";
        $this->load->helper("text");
        if(is_array($notifications) and count($notifications))
        {
          foreach($notifications as $i => $n)
          {
            
            $status = $n['status_owner'];
            $do_status = $status;
            $container_status = "";
            switch($status)
            {
              case "cancel":
              $do_status = "<b>membatalkan penawaran</b>";
              $is_read = ($this->gts_notification->is_read($n['id'],'bidding'))?'':' alert-danger';
              $container_status = "class='alert".$is_read."'";
              break;
              case "approve" :
              $do_status = "<b>menyetujui penawaran</b>";
              $is_read = ($this->gts_notification->is_read($n['id'],'bidding'))?'':' alert-success';
              $container_status = "class='alert".$is_read."'";
              break;
              case "" :
              $do_status = "<b>melakukan penawaran</b>";
              $is_read = ($this->gts_notification->is_read($n['id'],'bidding'))?'':' alert-info';
              $container_status = "class='alert".$is_read."'";
              break;
            }
            
            $messages = str_replace("  "," ",str_replace("\r"," ",str_replace("\n"," ",'Member '.$this->gts_members->str_user_code($n['member_id']).' '.$do_status.' pada sertifikat '.$n['no_certificate'].' Anda')));
      ?>
            <li>
              <a <?php echo $container_status;?> href="<?php echo base_url('certificates/bidding_detail/'.$n['selling_id'].'/show/'.$n['id']);?>">
                  <?php echo '<i class="small"><b>'.$n['date_updated'].'</b></i><br/>'. ' ' . wordwrap($messages,35,"<br/>\n",true);?>
              </a>
            </li>
      <?php      
          }
        }
      ?>
            <li>
              <a <?php echo $container_status;?> href="<?php echo base_url('notification/show_all');?>">
                  Tampilkan Semua Pemberitahuan
              </a>
            </li>
    </ul>
