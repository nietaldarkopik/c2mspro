<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
          <div class="col-sm-4 col-md-3 sidebar-container">
					
						<form action="#" method="get" class="form form-search clearfix">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i></button>
                </span>
              </div><!-- /input-group -->
            </form>
						
						<div class="list-group control-panel">
              <?php $this->load->view("components/menu");?>
              <!--
							<a href="<?php echo base_url('dashboard');?>" class="list-group-item <?php if(str_replace('/index.php','',current_url()) == base_url('dashboard') or str_replace('index.php','',current_url()) == base_url()) echo ' active ';?>">
                <i class="glyphicon glyphicon-user">&nbsp;</i>Home
              </a>
							<a href="<?php echo base_url('certificates/sell');?>" class="list-group-item <?php if(str_replace('/index.php','',current_url()) == base_url('certificates/sell')) echo ' active ';?>">
                <i class="glyphicon glyphicon-bullhorn">&nbsp;</i>Sell Certificates
              </a>
							<a href="<?php echo base_url('certificates/bid');?>" class="list-group-item <?php if(str_replace('/index.php','',current_url()) == base_url('certificates/bid')) echo ' active ';?>">
                <i class="glyphicon glyphicon glyphicon-screenshot">&nbsp;</i>Bid Certificates
              </a>
							<a href="<?php echo base_url('transaction/checkout');?>" class="list-group-item <?php if(str_replace('/index.php','',current_url()) == base_url('transaction/checkout')) echo ' active ';?>">
                <i class="glyphicon glyphicon-credit-card">&nbsp;</i>Money Transfer
              </a>
							<a href="<?php echo base_url('transaction/history');?>" class="list-group-item <?php if(str_replace('/index.php','',current_url()) == base_url('transaction/history')) echo ' active ';?>">
                <i class="glyphicon glyphicon-time">&nbsp;</i>History
              </a>
							<a href="<?php echo base_url('dashboard/do_logout');?>" class="list-group-item <?php if(str_replace('/index.php','',current_url()) == base_url('dashboard/logout')) echo ' active ';?>">
                <i class="glyphicon glyphicon glyphicon-log-out">&nbsp;</i>Logout
              </a>
              -->
						</div><!-- .panel-user -->
						
            <?php
            $this->load->view("components/widget_greenwallet");						
            $this->load->view("components/widget_certificates");
            ?>
					</div><!-- .sidebar-container -->
          
