<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-comment"></i></a>
  <ul class="dropdown-menu">
    <li class="dropdown-header">No private messages</li>
  </ul>
