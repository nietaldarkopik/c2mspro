<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
		<!-- HEADER START -->
		<div class="site-header">
      <nav class="navbar navbar-default" role="navigation">
        <div class="container">
        
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <h1 class="logo"><a title="GTSystem" class="navbar-brand" href="index.htmll">GreenTradingSystem</a></h1>
          </div>

          <div class="collapse navbar-collapse" id="gts-navigation">
            <ul class="nav navbar-nav navbar-right">
              <li class="badges_messages dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-comment"></i></a>
                <ul class="dropdown-menu">
                  <li class="dropdown-header">No private messages</li>
                </ul>
              </li>
              <!--
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i></a>
                <ul class="dropdown-menu">
                  <li class="dropdown-header">No Data</li>
                </ul>
              </li>
							-->
              <li class="badges_notification dropdown notification-on">
                  <?php
                    $this->load->view("components/badges_notification");
                  ?>
							</li>
							
              <li class="profile-menu dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo current_theme_url();?>assets/upload/user-40x40.jpg" alt="User" />Mr. Green <span class="caret"><img src="<?php echo current_theme_url();?>assets/images/small-down-arrow-dark.png" /></span></a>
                <ul class="dropdown-menu">
                  <li class="clearfix"><a href="single-profile.html"><i class="glyphicon glyphicon-user">&nbsp;</i>Profile</a></li>
                  <li class="clearfix"><a href="archive-posts.html"><i class="glyphicon glyphicon-envelope">&nbsp;</i>Inbox</a></li>
                  <li class="clearfix"><a href="single-post.html"><i class="glyphicon glyphicon-send">&nbsp;</i>Send Private Message</a></li>
                  <li class="clearfix"><a href="<?php echo base_url();?>dashboard/do_logout"><i class="glyphicon glyphicon-log-out">&nbsp;</i>Logout</a></li>
                </ul>
              </li>
            </ul>
          </div>
          
        </div>
      </nav>
		</div>
		<!-- HEADER END -->
		
