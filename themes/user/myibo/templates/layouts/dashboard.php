<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  echo $this->block->call_block("left");
?>

    <!--/popup-->
    <section class="home-content" role="main">
        <div class="wrapper">
            <div class="container">

                <div class="product-content">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="font-family:arial_black; color:#656565; text-align:center;">
                            <h2><strong>PAKET PRODUK OBI</strong></h2>
                        </div>
                    </div>
                    <div class="clearfix spacer"></div>
                    <div class="row">
                        <div class="ui-blurbs">

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="blurb blurb1">
                                    <div class="row">
                                        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <img src="<?php echo current_theme_url();?>assets/images/mega.png" style="margin:40px; width:230px; height:100px;">
                                            <h4><font color="black">Harga Konsumen	Rp 1,3 Juta</font></h4>
                                            <h4><font color="green">Harga Funbizzer	Rp 1,1 Juta</font></h4>
                                            <p>Mendapatkan:
                                                <br>• Workshop basic
                                                <br>• Starter kit untuk keanggotaan Funbizz</p>
                                            <a href="#"><img src="<?php echo current_theme_url();?>assets/images/btn_buy1.png" style="margin:50px 63px 0; width:170px;">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="blurb blurb1">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <img src="<?php echo current_theme_url();?>assets/images/tera.png" style="margin:40px; width:190px; height:100px;">
                                            <h4><font color="black">Harga Konsumen	Rp 4,9 Juta</font></h4>
                                            <h4><font color="darkred">Harga Funbizzer	Rp 3,9 Juta</font></h4>
                                            <p>Mendapatkan:
                                                <br>• Workshop basic
                                                <br>• Workshop advance
                                                <br>• Website profesional
                                                <br>• Domain
                                                <br>• Paket trial untuk keanggotaan Funbizz</p>
                                            <a href="#"><img src="<?php echo current_theme_url();?>assets/images/btn_buy2.png" style="margin:2px 65px 0; width:170px;">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="blurb blurb1">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <img src="<?php echo current_theme_url();?>assets/images/zetta.png" style="margin:40px; width:230px; height:100px;">
                                            <h4><font color="black">Harga Konsumen	Rp 12,9 Juta</font></h4>
                                            <h4><font color="darkblue">Harga Funbizzer	Rp 9,9 Juta</font></h4>
                                            <p>Mendapatkan:
                                                <br>• Workshop premium
                                                <br>• Website profesional
                                                <br>• Domain
                                                <br>• Optimasi funpage OBI
                                                <br>• Paket pro untuk keanggotaan Funbizz</p>
                                            <a href="#"><img src="<?php echo current_theme_url();?>assets/images/btn_buy3.png" style="margin:3px 63px 0; width:170px;">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix spacer"></div>
                        </div>
                    </div>

                    <div class="clearfix spacer"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- Home page Content -->
    <!-- Footer -->
    <div id="cur-day" style="display:none;"> Thursday </div>

<?php
  $this->load->view("footer");
?>
