<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  if($is_ajax == 1){}else{
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $this->master->get_value(TABLE_SYS_SETTINGS,"value",array("setting_id" => '3'));?>">
    <meta name="author" content="<?php echo $this->master->get_value(TABLE_SYS_SETTINGS,"value",array("setting_id" => '4'));?>">
    <meta name="keywords" content="<?php echo $this->master->get_value(TABLE_SYS_SETTINGS,"value",array("setting_id" => '5'));?>">
    <link rel="shortcut icon" href="<?php echo current_admin_theme_url();?>dist/ico/favicon.ico">

    <title><?php echo $this->master->get_value(TABLE_SYS_SETTINGS,"value",array("setting_id" => '2'));?></title>
    <?php echo $this->assets->print_css("head");?>
    <script type="text/javascript">
       var base_url = "<?php echo base_url();?>";
       var current_url = "<?php echo current_url();?>";
       var current_theme_url = "<?php echo current_admin_theme_url();?>";
       var theme_name = "<?php echo CURRENT_THEME;?>";
    </script>
    <?php echo $this->assets->print_js("head");?>
    <style type="text/css">
      <?php echo $this->assets->print_css_inline("head");?>
    </style>
    <script type="text/javascript">
      <?php echo $this->assets->print_js_inline("head");?>
    </script>
  </head>

  <body role="document">
	<header class="header">
		<div class="secondary">
			<div class="wrapper">
				<div class="container">
					<div class="row">

						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-7">
							<div class="logo">
                <?php
                  $default_content = '<a href="'.base_url().'">
                                        <img src="' . current_theme_url() . 'assets/images/obi-logo.png" />
                                      </a>';
                  echo $this->master->get_inline_edit_content($this->uri->segment(1),"%","%","top","auto",$default_content);
                ?>
							</div>
						</div>

						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-2">
							<div class="row">
								<nav class="nav" style="margin-top:20px;">
								  <form action="#" method="post">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3 pull-right">
									  <ul>
										<li>
											<input class="login" type="text" name="username" placeholder="user name"></input>
										</li>
										<li>
											<input class="login" type="password" name="password" placeholder="password"></input>
										</li>
										<li>
											<input type="submit" value="Log in" class="ui-button"></input>
										</li>
										</ul>
									</div>
								</form>
								</nav>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- / -->
	</header>

    <!-- homepage slider -->
    <section class="masthead hidden-sm hidden-xs" role="slider">
        <ul id="slider">
            <!-- Slider 1 -->
            <li class="slider1">
                <div class="wrapper">
                    <div class="container">

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h2>
                                <span><div align="center"> Memulai bisnis online atau meningkatkan omzet bisnis Anda hanya dengan</div></span> 
                                <strong>
                                    <img src="<?php echo current_theme_url();?>assets/images/line-sep-flipped.png" />
                                    <span style="padding: 0 30px;">5 Langkah Mudah</span>
                                    <img src="<?php echo current_theme_url();?>assets/images/line-sep.png" />
                                </strong>
                            </h2>
                            </div>
                        </div>

                        <div class="row">
                            <ul class="rc_program_list">
                                <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">
                                    <a href="#">
                                        <li class="step1">
                                        </li>
                                    </a>
                                </div>
                            </ul>
                        </div>

                        <div class="spacer hidden-sm hidden-xs"></div>

                    </div>
                </div>
            </li>
            <!-- /Slider 1 -->

            <!-- Slider 2 -->
            <li class="slider slider2">
                <div class="wrapper">
                    <div class="container">
                        <br>
                        <br>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h2><span>Memiliki banyak pilihan desain menarik, kreatif, dan tematik sesuai pilihan Anda</h2>
                            </div>
                        </div>

                        <div class="row">
                            <br>
                            <br>
                            <br>
                            </h2>
                        </div>
                    </div>

                    <div class="row">
                        <ul class="rc_program_list">
                            <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">
                                <a href="#">
                                    <li class="step2">
                                    </li>
                                </a>
                            </div>
                        </ul>
                    </div>

                    <div class="spacer hidden-sm hidden-xs"></div>
                </div>
            </li>
            <!-- /Slider 2 -->

            <!--  Slider 3 -->
            <li class="slider3">
                <div class="wrapper">
                    <div class="container">

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h2>
                                <span> OBI menggunakan sistem user friendly sehingga website dapat di edit dengan mudah</span> 
                            </h2>
                            </div>
                        </div>

                        <div class="row">
                            <ul class="rc_program_list">
                                <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">
                                    <a href="#">
                                        <li class="step3">
                                        </li>
                                    </a>
                                </div>
                            </ul>
                        </div>

                        <div class="spacer hidden-sm hidden-xs"></div>

                    </div>
                </div>
            </li>
            <!--  Slider 3 -->

            <!--  Slider 4 -->
            <li class="slider4">
                <div class="wrapper">
                    <div class="container">

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h2>
                                <span> Website OBI dapat di buka di berbagai smartphone <br>
(mobile compatibility)</span> 
                            </h2>
                            </div>
                        </div>

                        <div class="row">
                            <ul class="rc_program_list">
                                <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">
                                    <a href="#">
                                        <li class="step4">
                                        </li>
                                    </a>
                                </div>
                            </ul>
                        </div>

                        <div class="spacer hidden-sm hidden-xs"></div>

                    </div>
                </div>
            </li>
            <!--  Slider 4 -->

            <!--  Slider 5 -->
            <li class="slider5">
                <div class="wrapper">
                    <div class="container">

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h2>
                                <span> Membuat blog Anda sendiri dengan mudah dan professional</span> 
                            </h2>
                            </div>
                        </div>

                        <div class="row">
                            <ul class="rc_program_list">
                                <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">
                                    <a href="#">
                                        <li class="step5">
                                        </li>
                                    </a>
                                </div>
                            </ul>
                        </div>

                        <div class="spacer hidden-sm hidden-xs"></div>

                    </div>
                </div>
            </li>
            <!--  Slider 5 -->

            <!--  Slider 6 -->
            <li class="slider6">
                <div class="wrapper">
                    <div class="container">

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h2>
                                <span> Cara instant memiliki toko online dan hasilkan omzet miliaran</span> 
                            </h2>
                            </div>
                        </div>

                        <div class="row">
                            <ul class="rc_program_list">
                                <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">
                                    <a href="#">
                                        <li class="step6">
                                        </li>
                                    </a>
                                </div>
                            </ul>
                        </div>

                        <div class="spacer hidden-sm hidden-xs"></div>

                    </div>
                </div>
            </li>
            <!--  Slider 6 -->
        </ul>
    </section>
    <section class="hidden-lg hidden-md">
        <div class="mini_slider_bg_image">
            <div class="wrapper">
                <div class="container">
                    <div class="row">
                        <div>
                            <h2><span>Join our Reseller Program for the</span> <br/><br/> <strong>Best Prices in the Industry</strong></br/></br/></h2>
                            <a href="#" class="ui-button-yellow" style="">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / homepage slider -->
    <!-- Home page Content -->
    <!--popup-->

    <div class="modal fade bs-example-modal-sm" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="mySmallModalLabel">Small popup</h4>
                </div>
                <div class="modal-body">
                    Jika dialog ini muncul dan Anda bisa baca tulisan ini berarti popup berhasil di load.
                </div>
            </div>
        </div>
    </div>

    <!--/popup-->
<?php } ?>
