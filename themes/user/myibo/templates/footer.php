<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  if($is_ajax == 1){}else{
?>
    <div class="pre-footer-bg hidden-sm hidden-xs">
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 hidden-xs">
                        <div class="pre-footer-text">
						<?php
						  $default_content = '
							<p class="text-line-2">Tingkatkan Omzet Bisnis Anda
                                <span style="font-weight: 900;">Sekarang </span>
                                <br /> dengan memulai Bisnis Online Anda di
                                <span style="font-weight: 900;">OBI</span>
                            </p>';
						  echo $this->master->get_inline_edit_content($this->uri->segment(1),"%","%","tingkatkan_omzet","auto",$default_content);
						  ?>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">
                        <a href="#">
                            <span class="rfloat btn-blu-longshadow" style=""></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <footer class="footer" role="contentinfo" style="background-color:#dbdbdb;">

        <div class="footer-bottom">
            <div class="wrapper" style="position:relative;z-index:-0;">
                <div class="links">
                    </br>
                </div>
                <div class="copyright-info">
					<?php
					  $default_content = '
                    <a href="#"><img class="lfloat" src="'.current_theme_url().'assets/images/ic-pay-option.gif" alt="Payment Options" />
                    </a>
                    <p class="cp-info rfloat" style="color:#000;">&copy;2015 Online Business Instant
                        <br /> All Rights Reserved</p>';
					  echo $this->master->get_inline_edit_content($this->uri->segment(1),"%","%","copy_right2","auto",$default_content);
					?>
                    <div class="clearfix spacer"></div>
                </div>
            </div>
        </div>

        <div class="wrapper hide" style="position:relative;z-index:-0;">
            <div class="rc-bg"> </div>
            <div class="container">

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-xs">
                        <div class="links">
                            <a href="#">Domain Reseller</a>
                            <a href="#">Compare Us</a>
                            <a href="#" target="_blank">Blog</a>
                            <a href="#">Payment Methods</a>
                            <a href="#" class="hidden-sm hidden-xs">Resource Center</a>
                            <a href="#">FAQ</a>
                            <a href="#" target="_blank">KnowledgeBase</a>
                            <a href="#">Privacy Policy</a>
                            <a href="#">Contact Us</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 ">
                        <div class="copyright-info">
                            <a href="#"><img src="<?php echo current_theme_url();?>assets/images/ic-pay-option.gif" alt="Payment Options" />
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<?php
						  $default_content = '
                        <p class="cp-info">&copy;2015 ResellerClub<br /> All Rights Reserved</p>';
						  echo $this->master->get_inline_edit_content($this->uri->segment(1),"%","%","copy_right","auto",$default_content);
						?>
                    </div>
                    <div class="clearfix spacer"></div>
                </div>
            </div>
        </div>

        <!-- HTML FOR THE CHAT START -->
        <!-- Added By : taufik ferdiansyah -->
        <!-- Modified By : taufik ferdiansyah -->
        <!-- Rev : 03 -->
        <div id="chat_handle" class="chat-handle hidden-sm hidden-xs" style=""></div>
        <div class="chat-window-1" style="display:none;">
            <div class="close-holder"> <span class="ic-close btn_close"> </span> </div>
            <div class="wrapper">
                <div>
					<?php
					  $default_content = '
						<p class="message"> Help us to serve you better,
							<br />
							<strong> Please select </strong>
                        </p>';
					  echo $this->master->get_inline_edit_content($this->uri->segment(1),"%","%","serve_you_better","auto",$default_content);
					?>
                    <p class="buttons"> <a id="btn_message" href="#" class="ui-button-yellow" style="text-transform:none;">Send Message</a> <span class="ic-chat-or"> </span> <a id="btn_chat" href="#" class="ui-button" style=" text-transform:none;">Chat</a> </p>
                </div>
            </div>
        </div>

        <div class="chat-window-2" style="display:none;">
            <div class="close-holder"> <span class="ic-close btn_close"> </span> </div>
            <div class="wrapper">
                <div style="position: relative; ">
                    <div class="online_section">
						<?php
						  $default_content = '
                        <p class="message-window1"> Help us to serve
                            <br /> you better, <strong> Please Fill Details </strong> </p>
                        <p class="sub-message-window1" style="color: #DDDDDD;"> Sales Team is available on <strong style="color: #fff;"> Weekdays from 5 AM to 7 PM GMT </strong>
                        </p>
                        <p class="sub-message-window1"> Note: Only our Pre-Sales team is available on chat. If you are an existing Reseller, you are requested to fill the below contact form OR send a mail to \'clientrelations@resellerclub.com\'.</p>
                        <p class="sub-message-window1"> For Technical queries, you can raise a Support Ticker from <a href="#"> here </a>Technical Support Team is available 24/7 </p>';
						  echo $this->master->get_inline_edit_content($this->uri->segment(1),"%","%","chat_footer","auto",$default_content);
						?>
                        <div class="chat-query-form">
							<span id="submit_result" class="hide clear alert text-muted bg-success padding0"></span>
                            <form id="query_form" method="POST" action="<?php echo base_url();?>services/plugin_messages/do_send" class="ajax" data-target-ajax=".chat-query-form" data-target=".chat-query-form">
                                <table class="chat-query-tbl">
                                    <tr>
                                        <td>
                                            <input id="chat_name" name="data[name]" type="text" placeholder="Name" class="frm-field required " style="width:180px" />
                                        </td>
                                        <td>
                                            <input id="chat_email" name="data[email]" type="text" placeholder="Email" class="frm-field required email" style="width:180px" />
                                        </td>
                                        <td>
                                            <input id="chat_phone" name="data[phone]" type="text" class="frm-field required " placeholder="Phone No." style="width:180px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <textarea id="chat_query" name="data[message]" placeholder="Message" class="frm-field required" style="width: 615px;height: 80px;"> </textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="text-align: right;">
											<span id="progress_loader" class="progress_loader"></span>
                                            <input type="hidden" name="nonce" value="<?php echo time();?>" />
											<input name="ajax_target" value=".chat-query-form" type="hidden">
											<input type="hidden" name="is_ajax" value="1"/>
                                            <input type="submit" name="submit" class="ui-button" value="Send Query" />
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                    <div class="offline_section">
						<?php
						  $default_content = '
										<p class="message-window1" style="width:100%;"> <strong> Our Sales Team is unavailable on weekends. </strong> </p>
										<p class="sub-message-window1-offline" style="width:100%;"> If you are interested in joining us, you can email your details to \'sales@resellerclub.com\'
											<br /> If you are an existing Reseller and wish to speak to our Client Relations team, you can send an email to \'clientrelations@resellerclub.com\'
											<br />
											<br /> If you require any urgent assistance, please contact our Support Team by raising a Support Ticket or calling us on one of the following numbers:
											<br /> Tel (USA): +1 (415) 236 1970
											<br /> Tel (UK): +44 (20) 7183 2151
											<br /> Tel (India): +91 22 3079 7676
											<br />
										</p>';
						  echo $this->master->get_inline_edit_content($this->uri->segment(1),"%","%","offline_section","auto",$default_content);
						?>
                    </div>
                </div>
            </div>
        </div>

        <!-- HTML FOR THE CHAT END -->

    </footer>


    <!-- HTML FOR SCREEN BLUR -->
    <div class="screen-blur" style="display:none;"> </div>
    <!-- HTML FOR SCREEN BLUR END -->

    <!-- HTML FOR GOTOTOP -->
    <div id="backtotop" class="btn_totop" style="display:none;">
        <a href="#" class="backtotop">Back to Top</a>
    </div>
    <?php #echo $this->assets->print_js("body");?>
    <script type="text/javascript">
      <?php echo $this->assets->print_js_inline("body");?>
    </script>
  </body>
</html>
<?php } ?>
