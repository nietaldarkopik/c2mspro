<?php

class myibo{
  var $CI;
  
  function __construct()
  {
    $this->CI =& get_instance();
  }
  
  function setAssets()
  {
    $css = '
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/stylesheet/node.css" />
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/stylesheet/defaults.css" />
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/stylesheet/system.css" />
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/stylesheet/system-menus.css" />
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/stylesheet/user.css" />
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/stylesheet/content-module.css" />
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/stylesheet/date.css" />
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/stylesheet/datepicker.css" />
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/stylesheet/jquery.timeentry.css" />
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/stylesheet/filefield.css" />
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/stylesheet/fieldgroup.css" />
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/stylesheet/bootstrap.css" />
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/stylesheet/emma.css" />
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/stylesheet/animate.css" />
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/stylesheet/colorbox.css" />
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/stylesheet/anythingslider.css" />
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/stylesheet/last-style.css" />
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/css/bootstrap-theme-admin.css">';
    
    $js_head = '
    <script type="text/javascript" src="'.current_theme_url().'assets/script/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/script/bootstrap.min.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/script/respond.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/script/anythingslider.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/script/anythingslider-fx.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/script/jquery.innerfade.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/script/common.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/script/jquery.validate.min.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/script/jquery.scrollbox.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/script/uitablefilter.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/jquery/tinymce_1.4/tinymce.min.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/jquery/tinymce_1.4/jquery.tinymce.min.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/js/jquery.form.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/js/ajax.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/js/data.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/js/blocks.js"></script>';
    $user_level = $this->CI->user_access->get_level();
    if($user_level == '1')
    {
		$js_head .= '<script type="text/javascript" src="'.current_theme_url().'assets/js/script.js"></script>';
	}
    
    $this->CI->assets->add_css($css,"head");
    $this->CI->assets->add_js($js_head,"head");
    
    $js = '';
    $this->CI->assets->add_js($js,"body");
  }
}
