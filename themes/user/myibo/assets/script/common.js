// JavaScript Document
$(document).ready(function(e) {

	
	// Open tabs on click of product attributs

	$(".tab_pf_link").click(function(e){
		e.preventDefault();		

		$('html, body').animate({scrollTop: 1120}, 'slow');

		$('#tabs_pf a').removeClass('active'); 
		var tab_id = $(this).attr('tabid');	
              
                $('#tabs_pf a[tabid="'+tab_id+'"]').addClass('active');
                 
    	
                fordiv2 = $(this).attr("href");
		$('#tab_content_pf').children('.tab-content1').hide();	
							
                $(fordiv2).fadeIn(500);

	});


	var slider = ['Free Storefront', 'Control Panel', 'HTTP API', 'WHMCS Integration'];
		function formatText(index, panel) {
			return slider[index - 1];
		}		
		$('#slider2').anythingSlider({
		autoPlay: true,
		hashTags : false,					
		buildArrows: false,
		delay: 5000,
		navigationFormatter : formatText,		
		buildStartStop: false
	});

	var slider_text3 = ['700+ TLDs to Sell', 'Financial Management', 'SMS Alerts & Notifications', 'Industry Best Prices', '24x7 Support Team'];
	function formatText3(index, panel) 
	{
		return slider_text3[index - 1];
	}		
	$('#slider3').anythingSlider({
		autoPlay: true,
		hashTags : false,					
		buildArrows: false,
		delay: 5000,
		navigationFormatter : formatText3,		
		buildStartStop: false
	});
	$('#slider4').anythingSlider({
		autoPlay: true,
		hashTags : false,					
		buildArrows: false,
		delay: 5000,
		navigationFormatter : formatText3,		
		buildStartStop: false
	});
	$('#slider5').anythingSlider({
		autoPlay: true,
		hashTags : false,					
		buildArrows: false,
		delay: 5000,
		navigationFormatter : formatText3,		
		buildStartStop: false
	});
	$('#slider6').anythingSlider({
		autoPlay: true,
		hashTags : false,					
		buildArrows: false,
		delay: 5000,
		navigationFormatter : formatText3,		
		buildStartStop: false
	});


		$("#tab_all").click(function(){
		$(".dezex-block").show();
		});	 
	
	    	$("#tab_des").click(function(){	 
		  $(".dezex-block").hide();
		  $(".ser_designer").fadeIn("slow");		    
		});
	
	    	$("#tab_dev").click(function(){	
		  $(".dezex-block").hide();
		  $(".ser_developer").fadeIn("slow");			
		});

	 	$("#tab_mar").click(function(){		  
		  $(".dezex-block").hide();
		  $(".ser_marketing").fadeIn("slow");
		});  

	 	$("#tab_sem").click(function(){
		  $(".dezex-block").hide();
		  $(".ser_sem").fadeIn("slow");		
	  	});   
	

	    	   









    /* JS FOR THE SALES CHAT - START */
	/* ADDED BY : Sandeep Pandita -> 29-07-2014 */
	/* MODIFIED  BY : -- */
	/* Rev : 01 */
	$("#chat_handle").click(function(e){
		$(".chat-window-1").slideToggle();
		$(".screen-blur").fadeIn('slow');
	});
	$(".btn_close").click(function(e){
	  	$(".chat-window-1").fadeOut();
	  	$(".chat-window-2").fadeOut();
		$(".screen-blur").fadeOut('slow');
	});
	$("#btn_message").click(function(e){
	  	e.preventDefault();
	  
	  	$(".chat-window-2").show();
	  	$(".chat-window-1").hide();
	});
	/*$("#btn_chat").click(function(e){
	   	e.preventDefault();
	  
	  	$(".chat-window-3").show();
	  	$(".chat-window-1").hide();
	});*/
	/* ---- */

	$('.os-linux').click(function(){
		$(this).parents('.os-sel').children('span').removeClass('active');		
		$(this).next('.on-off').animate({left:'3px'},100);
		$(this).addClass('active');
		$(this).parent('.os-sel').parent('.plan').children('.plan-heading').children('.plan-option').animate({left:'0px'},400);

	});
	$('.os-win').click(function(){
		$(this).parents('.os-sel').children('span').removeClass('active');		
		$(this).prev('.on-off').animate({left:'76px'},100);
		$(this).addClass('active');
		$(this).parent('.os-sel').parent('.plan').children('.plan-heading').children('.plan-option').animate({left:'-268px'},400);		

	});
	
	$('.in-dd-option').click(function(){
		$(this).parent('.opt-location').parent('.widget-loc-sel').parent('.loca-sel').parent('.plan').children('.plan-heading').children('.plan-option').children('.all-plans').animate({top:'-150px'},400);
		$(this).parent('.opt-location').parent('.widget-loc-sel').parent('.loca-sel').parent('.plan').children('.plan-details').children('.sub-plan-details').children('.sub-plan-item').animate({top:'-5px'},400); // for Band 1 - Band 4
		$(this).parent('.opt-location').parent('.widget-loc-sel').parent('.loca-sel').parent('.plan').children('.plan-details').children('.sub-plan-details-r').children('.sub-plan-item-r').animate({top:'-45px'},400); // for R1 - R4
		});
	$('.us-dd-option').click(function(){
		$(this).parent('.opt-location').parent('.widget-loc-sel').parent('.loca-sel').parent('.plan').children('.plan-heading').children('.plan-option').children('.all-plans').animate({top:'0'},400);
		$(this).parent('.opt-location').parent('.widget-loc-sel').parent('.loca-sel').parent('.plan').children('.plan-details').children('.sub-plan-details').children('.sub-plan-item').animate({top:'-5px'},400);  // for Band 1 - Band 4
		$(this).parent('.opt-location').parent('.widget-loc-sel').parent('.loca-sel').parent('.plan').children('.plan-details').children('.sub-plan-details-r').children('.sub-plan-item-r').animate({top:'-5px'},400); // for R1 - R4
		});
	$('.uk-dd-option').click(function(){
		$(this).parent('.opt-location').parent('.widget-loc-sel').parent('.loca-sel').parent('.plan').children('.plan-heading').children('.plan-option').children('.all-plans').animate({top:'-300px'},400);
		$(this).parent('.opt-location').parent('.widget-loc-sel').parent('.loca-sel').parent('.plan').children('.plan-details').children('.sub-plan-details').children('.sub-plan-item').animate({top:'-5px'},400);  // for Band 1 - Band 4
		$(this).parent('.opt-location').parent('.widget-loc-sel').parent('.loca-sel').parent('.plan').children('.plan-details').children('.sub-plan-details-r').children('.sub-plan-item-r').animate({top:'-83px'},400); // for R1 - R4		
		});
	$('.tr-dd-option').click(function(){
		$(this).parent('.opt-location').parent('.widget-loc-sel').parent('.loca-sel').parent('.plan').children('.plan-heading').children('.plan-option').children('.all-plans').animate({top:'-450px'},400);	
		$(this).parent('.opt-location').parent('.widget-loc-sel').parent('.loca-sel').parent('.plan').children('.plan-details').children('.sub-plan-details').children('.sub-plan-item').animate({top:'-45px'},400);  // for Band 1 - Band 4
		$(this).parent('.opt-location').parent('.widget-loc-sel').parent('.loca-sel').parent('.plan').children('.plan-details').children('.sub-plan-details-r').children('.sub-plan-item-r').animate({top:'-120px'},400); // for R1 - R4		
		});
	$('.hk-dd-option').click(function(){
		$(this).parent('.opt-location').parent('.widget-loc-sel').parent('.loca-sel').parent('.plan').children('.plan-heading').children('.plan-option').children('.all-plans').animate({top:'-600px'},400);	
		$(this).parent('.opt-location').parent('.widget-loc-sel').parent('.loca-sel').parent('.plan').children('.plan-details').children('.sub-plan-details').children('.sub-plan-item').animate({top:'-80px'},400);	
		});
		
		
	$('.uk3-dd-option').click(function(){
		$(this).parent('.opt-location').parent('.widget-loc-sel').parent('.loca-sel').parent('.plan').children('.plan-heading').children('.plan-option').children('.all-plans')
		.animate({top:'-750px'},400);		
		});
			
		
	

	$('.loc-dd-band1').click(function() { // 
        $(this).next(".opt-location").slideDown(300);
    });
	$('.loc-dd-band2').click(function() { // cutome location dd
        $(this).next(".opt-location").slideDown(300);
    });
	$('.loc-dd-band3').click(function() { // cutome location dd
        $(this).next(".opt-location").slideDown(300);
    });
	$('.loc-dd-band4').click(function() { // cutome location dd
        $(this).next(".opt-location").slideDown(300);
    });
	
	$('.opt-location > li').click(function(){
		var opt = $(this).html();
		$(this).parent('ul').prev('.loc-dd-band').html(opt);
		$(this).parent('ul').hide();
	});
	
	$('body').click(function(){
		if ($('.opt-location-band1').height()>1) {$('.opt-location-band1').slideUp(300);};
		if ($('.opt-location-band2').height()>1) {$('.opt-location-band2').slideUp(300);};
		if ($('.opt-location-band3').height()>1) {$('.opt-location-band3').slideUp(300);};
		if ($('.opt-location-band4').height()>1) {$('.opt-location-band4').slideUp(300);};
	});
	
	
	$('.loc-dd').click(function() { // cutome location dd
        $(this).next(".opt-location").toggle(300);
    });
	
	$('.opt-location > li').click(function(){
		var opt = $(this).html();
		$(this).parent('ul').prev('.loc-dd').html(opt);
		$(this).parent('ul').hide();
	});

	//show/hide top country list
    $('#toggle-cclist').click(function(){
		$(this).addClass('has-selected');
		$('#country-dd').slideToggle(300);	
		$('#call-dd').slideUp(300);	
	});
	
	/* scroll */
	$('#gtld-scroll').scrollbox({
 	});
	/* /scroll */
	
	/* scroll */
	$('#pd-scroll').scrollbox({
		});
	/* /scroll */
	
	
	/* pricing-sub-tabs */
	$('#btn-codeguard').click(function(){
		$('#cd-content').show();
		
		$('#ssl-content').hide();
		$('#sl-content').hide();

		$('#btn-codeguard').addClass('active');		
		$('#btn-ssl').removeClass('active'); 
		$('#btn-sitelock').removeClass('active');  
	});
	$('#btn-sitelock').click(function(){
		$('#sl-content').show();

		$('#ssl-content').hide();
		$('#cd-content').hide();

		$('#btn-sitelock').addClass('active');		
		$('#btn-ssl').removeClass('active');
		$('#btn-codeguard').removeClass('active');
	});
	$('#btn-ssl').click(function(){
		$('#ssl-content').show();

		$('#sl-content').hide();
		$('#cd-content').hide();

		$('#btn-ssl').addClass('active');	
		$('#btn-sitelock').removeClass('active');
		$('#btn-codeguard').removeClass('active');	
	});
	$('#btn-per-email').click(function(){
		$('#pemail-content').show();
		$('#eemail-content').hide();
		$('#btn-per-email').addClass('active');		
		$('#btn-ent-email').removeClass('active');
	});
	$('#btn-ent-email').click(function(){
		$('#eemail-content').show();
		$('#pemail-content').hide();
		$('#btn-ent-email').addClass('active');	
		$('#btn-per-email').removeClass('active');	
	});
	$('#btn-ds').click(function(){
		
		$('.hosting-content').hide();
		$('#ds-content').show();
		$('.btn-hosting').removeClass('active');
		$(this).addClass('active');	
	});
	
	$('#btn-ms').click(function(){
		
		$('.hosting-content').hide();
		$('#ms-content').show();
		$('.btn-hosting').removeClass('active');
		$(this).addClass('active');	
	});
	
	$('#btn-vps').click(function(){
		
		$('.hosting-content').hide();
		$('#vps-content').show();
		$('.btn-hosting').removeClass('active');
		$(this).addClass('active');	
	});
	$('#btn-rh').click(function(){
		
		$('.hosting-content').hide();
		$('#rh-content').show();
		$('.btn-hosting').removeClass('active');
		$(this).addClass('active');	
	});
	$('#btn-sh').click(function(){
		
		$('.hosting-content').hide();
		$('#sh-content').show();
		$('.btn-hosting').removeClass('active');
		$(this).addClass('active');	
	});
	$('#btn-sh1').click(function(){
		
		$('.hosting-content').hide();
		$('#sh-content1').show();
		$('.btn-hosting').removeClass('active');
		$(this).addClass('active');	
	});
	
	
	
	/* /pricing-sub-tabs */
	
	
	/* nav menu slide-down */
	$('#nav-domain').click(function(){
		$(this).children('a').addClass('active');
		$('#nav-domain-content').slideToggle(300);	
		$('#nav-hosting-content').slideUp(300);	
		$('#nav-addons-content').slideUp(300);	
		$('#nav-reseller-content').slideUp(300);	
	});
	$('#nav-hosting').click(function(){
		$(this).children('a').addClass('active');
		$('#nav-domain-content').slideUp(300);	
		$('#nav-hosting-content').slideToggle(300);	
		$('#nav-addons-content').slideUp(300);	
		$('#nav-reseller-content').slideUp(300);	
	});
	$('#nav-addons').click(function(){
		$(this).children('a').addClass('active');
		$('#nav-domain-content').slideUp(300);	
		$('#nav-hosting-content').slideUp(300);	
		$('#nav-addons-content').slideToggle(300);	
		$('#nav-reseller-content').slideUp(300);	
	});
	
$('#nav-reseller').click(function(){
		$(this).children('a').addClass('active');
		$('#nav-domain-content').slideUp(300);	
		$('#nav-hosting-content').slideUp(300);	
		$('#nav-addons-content').slideUp(300);	
		$('#nav-reseller-content').slideToggle(300);	
	});
	
	
	
	
	/*$('.nav li a').click(function(){
		$('.nav li a').removeClass('active');
		$(this).addClass('active');
		$('#nav-domain-content, #nav-hosting-content, #nav-addons-content, #nav-reseller-content').slideUp(600);	
	});
	
	$('#nav-domain').click(function(){
	$('#nav-domain-content').slideDown(600);
	});
	
	$('#nav-hosting').click(function(){
	$('#nav-hosting-content').slideDown(600);
	});
	
	$('#nav-addons').click(function(){
	$('#nav-addons-content').slideDown(600);
	});
	
	$('#nav-reseller').click(function(){
	$('#nav-reseller-content').slideDown(600);
	});*/
	
	
	
	/* /nav menu slide-down */
	
	/* search */
	var theTable = $('table.tbl-domains');
   
  $(".input-tlds").keyup(function() {
    $.uiTableFilter( theTable, this.value );
  })
  
   var theTable2 = $('table.tbl-new-gtlds');
   
  $(".input-new-gtlds").keyup(function() {
    $.uiTableFilter( theTable2, this.value );
  })
  
  /* /search */
	
	 $('body').click(function(){
		 
		if ($('#country-dd').height()>1) {$('#country-dd').slideUp(300);
		$('#toggle-cclist').removeClass("has-selected");
		};
		if ($('#call-dd').height()>1) {$('#call-dd').slideUp(300);
		$('#toggle-call').removeClass("has-selected");		
		};
		if ($('#nav-domain-content').height()>1) {$('#nav-domain-content').slideUp(300);
		$('#nav-domain').children('a').removeClass("active");		
		};
		if ($('#nav-hosting-content').height()>1) {$('#nav-hosting-content').slideUp(300);
		$('#nav-hosting').children('a').removeClass("active");		
		};
		if ($('#nav-addons-content').height()>1) {$('#nav-addons-content').slideUp(300);
		$('#nav-addons').children('a').removeClass("active");		
		};
		if ($('#nav-reseller-content').height()>1) {$('#nav-reseller-content').slideUp(300);
		$('#nav-reseller').children('a').removeClass("active");		
		};
	
	
	})
	
	
	$('#toggle-call').click(function(){
		$(this).addClass('has-selected');
		$('#call-dd').slideToggle(300);	
		$('#country-dd').slideUp(300);
		$('#toggle-cclist').addClass("has-selected");
	})
	
	
	//
	// features tab
	$(".tab").click(function(e){
		e.preventDefault();		
		$('.tabs a').removeClass('active');
		$(this).addClass('active');		
    	//$(".feature-content").hide();
    	fordiv = $(this).attr("href");
		//alert(fordiv);
		$('#tab-content').children('.tab-content').hide();	
		$('#payment-methods .tab-content').hide();					
    	$(fordiv).show();
	});
	
	/*$(".tab").click(function(e){     // blocks the click event on the tab,
		e.preventDefault();	           // because hover event used 
	});*/
    /*$(".tab").click(function(e){     // same thing but using the dotimeout jq plugin
		e.preventDefault();	
		
		$(this).doTimeout( 'click', 100, function(){
		// do something with text, like an ajax lookup
		
		$('.tabs a').removeClass('active');
		$(this).addClass('active');		
    	//$(".feature-content").hide();
    	fordiv = $(this).attr("href");
		//alert(fordiv);
		$('#tab-content').children('.tab-content').hide();	
		$('#payment-methods .tab-content').hide();					
    	$(fordiv).show();

		});
	});*/
	
	
	
	
	
	
	//payment-methods
	
	$(".pay-opt-tabs .tab").click(function(e){
			var h =$(".pay-opt-container").height();	
			
			if(h <= 600)
			{
			 $(".pay-opt-tabs .tabs").css('height', '600px');
			}
			else
			{
				$(".pay-opt-tabs .tabs").css('height', h + "px");
			}
	});		
	//------------------------------
	
	
	 $("#has-login").click(function(e){ // login option
		$("#login-blurb").toggle();		  
		e.stopPropagation();
	});	
	$("body").click( function(e) {
		$("#login-blurb").hide();
	});	

	

	
	
	$('#footer-clist-dd').click(function(){ // footer country list
		$('#footer-cc-list').slideToggle(200);
	});
	
	$('.accordian h4').click(function(){ //accordian
		$('.accordian h4').removeClass('active');
		$('.accordian .accordian-content').slideUp(250);
		$(this).addClass('active')
		$(this).next('.accordian-content').slideDown(250);
	})
	
	$('#testimonials').innerfade({ speed: 650, timeout: 4000, type: 'random', containerheight: '170px' }); 
	
	$(".pricing-table td:nth-child(1)").addClass("col1"); //table formating
	$(".pricing-table td:nth-child(2), .pricing-table td:nth-child(4), .pricing-table td:nth-child(6)").addClass("col-bg1");
	$(".pricing-table td:nth-child(3), .pricing-table td:nth-child(5), .pricing-table td:nth-child(7)").addClass("col-bg2");	
	
	$(".secondary-table td:nth-child(1)").addClass("col1"); //common table formating
	$(".secondary-table td:nth-child(2), .secondary-table td:nth-child(4), .secondary-table td:nth-child(6)").addClass("col-bg1");
	$(".secondary-table td:nth-child(3), .secondary-table td:nth-child(5), .secondary-table td:nth-child(7)").addClass("col-bg2");			

$(".compare-table td:nth-child(1)").addClass("col1");
	$(".compare-table td:nth-child(2)").addClass("col2"); 
	$(".compare-table td:nth-child(3)").addClass("col3"); 
	$(".compare-table td:nth-child(4)").addClass("col4"); 	

	$('.inner-tab').click(function(e){
		e.preventDefault();
		$(this).parent('li').parent('.inner-tabs').children('li').children('a').removeClass('active');
		$(this).addClass('active');
		fordiv = $(this).attr("href");
		$(fordiv).parent('.inner-tab-cont').children('.intab-content').hide();
		$(fordiv).fadeIn(500);
	})
	
});


$(document).ready(function(){			

		$('#stop_promo #promo').attr('id','sb-promos');
	
		$('#promo').anythingSlider({
		autoPlay: true,
		hashTags : false,					
		buildArrows: false,
		infiniteSlides: false,			
		delay: 4000,	
		buildStartStop: false
		})	
});

$(document).ready(function(){ //home page slider
	
		var slider = [ 'Easy Making' , 'Themes' , 'Simple Edit' , 'Mobile Compatibility' , 'Blogging' , 'E-commerce' ];
		function formatText(index, panel) {
			return slider[index - 1];
		}
		
	$(function(){  
		
		$('#slider').anythingSlider({
		expand : true,
		resizeContents : true, 
		autoPlay: true,
		hashTags : false,					
		buildArrows: false,
		infiniteSlides: false,			
		delay: 4000,
		navigationFormatter : formatText,		
		buildStartStop: false
		})	
		.anythingSliderFx({
			'.panel h2 span' : [ 'listLR' ],
			'.panel h2 strong' : [ 'listRL' ],
			'.dmn-promo-list li' : [ 'bottom'],
			'.host-promo-list li' : [ 'fade'],
			'.prog-feature-list' : [ 'listRL']
		});
	});
});


// for demo home page 1 - url is /hp1
$(document).ready(function(){ //home page slider
	
		var sliderhp1 = [ 'Themes' , 'Simple Edit' , 'Easy Making' , 'Mobile Compatibility' , 'Blogging' , 'E-commerce' ];
		function formatText(index, panel) {
			return sliderhp1[index - 1];
		}
		
	$(function(){
		
		$('#sliderhp1').anythingSlider({
		autoPlay: true,
		hashTags : false,					
		buildArrows: false,
		infiniteSlides: false,			
		delay: 4000,
		navigationFormatter : formatText,		
		buildStartStop: false
		})	
		.anythingSliderFx({
			'.panel h2 span' : [ 'listLR' ],
			'.panel h2 strong' : [ 'listRL' ],
			'.dmn-promo-list li' : [ 'bottom'],
			'.host-promo-list li' : [ 'fade'],
			'.prog-feature-list' : [ 'listRL']
		});
	});
});
// for demo home page 2 - url is /hp2
$(document).ready(function(){ //home page slider
	
		var sliderhp2 = [ 'Easy Making' , 'Simple Edit' , 'Themes' , 'Mobile Compatibility' , 'Blogging' , 'E-commerce' ];
		function formatText(index, panel) {
			return sliderhp2[index - 1];
		}
		
	$(function(){
		
		$('#sliderhp2').anythingSlider({
		autoPlay: true,
		hashTags : false,					
		buildArrows: false,
		infiniteSlides: false,			
		delay: 4000,
		navigationFormatter : formatText,		
		buildStartStop: false
		})	
		.anythingSliderFx({
			'.panel h2 span' : [ 'listLR' ],
			'.panel h2 strong' : [ 'listRL' ],
			'.dmn-promo-list li' : [ 'bottom'],
			'.host-promo-list li' : [ 'fade'],
			'.prog-feature-list' : [ 'listRL']
		});
	});
});




function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

$(document).ready(function(){
	
    $(window).scroll(function() {
				
        if($(window).scrollTop() > 604 )
        {
            $('#flt-black-row').addClass('scrollbar');
        }
        else if($(window).scrollTop() < 604 )
        {
            $('#flt-black-row').removeClass('scrollbar');
        }
    });

});

